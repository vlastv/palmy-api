<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Provider\Uuid;

$factory->define(Palmy\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'username' => $faker->unique()->userName,
        'email' => $faker->unique()->email,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'avatar' => $faker->imageUrl(),
    ];
});

$factory->define(\Palmy\Trip::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence(mt_rand(1, 6)),
        'end' => $date = $faker->optional()->dateTime,
        'start' => $faker->dateTime($date),
        'private' => $faker->boolean,
        'user_id' => function () {
            return factory(Palmy\User::class)->create()->id;
        }
    ];
});

$factory->define(\Palmy\Media::class, function (Faker\Generator $faker) {
    return [
        'name' => Uuid::uuid() . '.' . $faker->fileExtension,
        'mime_type' => $faker->mimeType,
        'provider_name' => 's3',
        'provider_reference' => function (array $media) {
            return $media['name'];
        },
        'context' => 'user_cover',
        'user_id' => function () {
            return factory(Palmy\User::class)->create()->id;
        }
    ];
});
