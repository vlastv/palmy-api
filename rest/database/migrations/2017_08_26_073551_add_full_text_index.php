<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFullTextIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('texts', function(Blueprint $schema) {
            $schema->morphs('searchable');
            $schema->text('text');
            $schema->text('filters');
        });

        DB::statement('ALTER TABLE texts ADD FULLTEXT search(text, filters)');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('ALTER TABLE texts DROP INDEX search');
    }
}
