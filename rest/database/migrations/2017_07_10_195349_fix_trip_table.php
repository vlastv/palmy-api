<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixTripTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->renameColumn('public', 'private');

            $table->dropColumn(['cover_url']);

            $table->unsignedInteger('place_id')->nullable()->change();
            $table->unsignedInteger('cover_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('trips', function (Blueprint $table) {
            $table->dropColumn(['cover_id']);

            $table->string('cover_url')->nullable();
            $table->unsignedInteger('place_id')->change();

            $table->renameColumn('private', 'public');
        });
    }
}
