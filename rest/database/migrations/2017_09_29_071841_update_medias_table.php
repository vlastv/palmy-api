<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMediasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->nullable()->change();
        });

        Schema::table('places', function (Blueprint $table) {
            $table->unsignedInteger('cover_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('media', function (Blueprint $table) {
            $table->unsignedInteger('user_id')->change();
        });

        Schema::table('places', function (Blueprint $table) {
            $table->dropColumn(['cover_id']);
        });
    }
}
