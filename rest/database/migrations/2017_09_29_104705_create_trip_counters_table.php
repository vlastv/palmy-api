<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('trip_id');
            $table->string('type_id');
            $table->unsignedInteger('value');
        });

        DB::update(<<<SQL
INSERT INTO trip_counters
(id, trip_id, type_id, value)
  SELECT
    NULL,
    trip_id,
    'place',
    COUNT(DISTINCT place_id)
  FROM posts
  WHERE trip_id IS NOT NULL
  GROUP BY trip_id
  UNION ALL
  SELECT
    NULL,
    trip_id,
    'photo',
    COUNT(*)
  FROM
    posts
    JOIN post_media ON posts.id = post_media.post_id
  WHERE trip_id IS NOT NULL
  GROUP BY trip_id
  UNION ALL
  SELECT
    NULL,
    trip_id,
    'participant',
    COUNT(*)
  FROM
    posts
    JOIN post_user ON posts.id = post_user.post_id
  WHERE trip_id IS NOT NULL
  GROUP BY trip_id
SQL
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_counters');
    }
}
