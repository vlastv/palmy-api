<?php

use Illuminate\Database\Seeder;

class TripsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        Palmy\User::inRandomOrder()
            ->each(function ($u) {
                factory(Palmy\Trip::class, mt_rand(0, 20))
                    ->make()
                    ->each(function ($t) use ($u) {
                        $t->user()->associate($u)->save();
                    });
            });
    }
}
