<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;
use Overtrue\LaravelFollow\Traits\CanBeSubscribed;
use Palmy\Contracts\Searchable as SearchableContract;
use Palmy\Traits\HasCounter;
use Palmy\Traits\Searchable;

class Tag extends Model implements SearchableContract
{
    use Sluggable;
    use CanBeSubscribed;
    use Searchable;
    use HasCounter;

    public $timestamps = false;

    public $fillable = ['name'];

    public function posts()
    {
        return $this->morphedByMany(Post::class, 'taggable');
    }

    public function getTextForSearch()
    {
        return [ltrim($this->name, '#')];
    }

    public function getFiltersForSearch()
    {
        return [];
    }

    public function getPrefix()
    {
        return 'hashtag';
    }

    public function usages()
    {
        return $this->counter()->where('type_id', 'usages');
    }
}
