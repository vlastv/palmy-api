<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    protected $fillable = ['provider', 'identifier', 'raw', 'name', 'avatar'];

    protected $casts = [
        'raw' => 'array'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
