<?php

namespace Palmy;

use Cog\Likeable\Contracts\HasLikes as Likeable;
use Cog\Likeable\Traits\HasLikes;
use Cog\Ownership\Contracts\HasOwner as HasOwnerContract;
use Cog\Ownership\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Palmy\Contracts\Commentable;
use Palmy\Contracts\Searchable as SearchableContract;
use Palmy\Traits\HasComments;
use Palmy\Traits\Resourcable;
use Palmy\Traits\Searchable;

class Post extends Model implements Likeable, HasOwnerContract, Commentable, SearchableContract
{
    use Resourcable, HasLikes;
    use HasOwner;
    use HasComments;
    use Taggable;
    use Searchable;

    protected $casts = [
        'private' => 'boolean'
    ];

    protected $fillable = ['message', 'private'];

    protected $defaults = [
        'private' => true,
    ];

    protected $ownerForeignKey = 'user_id';

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Post $post) {
            $post->photos()->detach();
            $post->participants()->detach();
            Item::onDeleting($post);
        });
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function photos(): BelongsToMany
    {
        return $this->belongsToMany(Media::class, 'post_media');
    }

    public function followers()
    {
        return $this->morphToMany(Item::class, 'item');
    }

    public function getTextForSearch()
    {
        return [
            $this->message
        ];
    }

    public function getFiltersForSearch()
    {
        return array_merge(
            $this->tags->all(),
            [
                $this->place,
                $this->trip,
                $this->user,
            ]
        );
    }

    public function getPrefix()
    {
        return 'post';
    }
}
