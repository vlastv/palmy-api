<?php

namespace Palmy;

use Hybridauth\User\Profile;
use Laravel\Passport\Bridge\User;
use Palmy\Social\Pipeline;

class UserProvider
{
    public function loadUserByProfile(Profile $profile, $provider)
    {
        $pipeline = new Pipeline();

        $user = $pipeline->process($provider, $profile);

        return new User($user->getAuthIdentifier());
    }
}
