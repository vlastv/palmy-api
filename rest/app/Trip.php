<?php

namespace Palmy;

use Cog\Likeable\Contracts\HasLikes as Likeable;
use Cog\Likeable\Traits\HasLikes;
use Cog\Ownership\Contracts\HasOwner as HasOwnerContract;
use Cog\Ownership\Traits\HasOwner;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Overtrue\LaravelFollow\Traits\CanBeSubscribed;
use Palmy\Contracts\Commentable;
use Palmy\Contracts\Searchable as SearchableContract;
use Palmy\Traits\HasComments;
use Palmy\Traits\Resourcable;
use Palmy\Traits\Searchable;

class Trip extends Model implements Likeable, HasOwnerContract, Commentable, SearchableContract
{
    use Resourcable, HasLikes;
    use CanBeSubscribed;
    use HasOwner;
    use HasComments;
    use Searchable;

    protected $casts = [
        'start' => 'date',
        'end' => 'date',
        'private' => 'boolean'
    ];

    protected $fillable = ['name', 'start', 'end', 'private'];

    protected $defaults = [
        'private' => true,
    ];

    protected $ownerForeignKey = 'user_id';

    protected static function boot()
    {
        parent::boot();

        static::deleting(function (Trip $trip) {
            $trip->posts()->update(['trip_id' => null]);
            $trip->participants()->detach();
            Item::onDeleting($trip);
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function place()
    {
        return $this->belongsTo(Place::class);
    }

    public function participants(): BelongsToMany
    {
        return $this->belongsToMany(User::class);
    }

    public function cover(): BelongsTo
    {
        return $this->belongsTo(Media::class);
    }

    public function scopeNewest(Builder $query, $direction = 'desc')
    {
        return $query
            ->where('end', '<=', new \DateTime('today'))
            ->orderBy('end', $direction);
    }

    public function scopePopular(Builder $query, $direction = 'desc')
    {
        return $query->inRandomOrder();
    }

    public function scopeDiscussable(Builder $query, $direction = 'desc')
    {
        return $query->inRandomOrder();
    }

    public function scopeEditorial(Builder $query, $direction = 'desc')
    {
        return $query->inRandomOrder();
    }

    public function getCoverUrlAttribute()
    {
        $media = $this->getRelationValue('cover');

        return $media ? $media['url'] : null;
    }

    public function followers()
    {
        return $this->morphToMany(Item::class, 'item');
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function getTextForSearch()
    {
        return [
            $this->name,
        ];
    }

    public function getFiltersForSearch()
    {
        return [
            $this->place,
            $this->user
        ];
    }

    public function getPrefix()
    {
        return 'trip';
    }

    private function counters()
    {
        return $this->hasOne(TripCounter::class);
    }

    public function photoCounter()
    {
        return $this->counters()->where('type_id', 'photo');
    }

    public function placeCounter()
    {
        return $this->counters()->where('type_id', 'place');
    }

    public function participantCounter()
    {
        return $this->counters()->where('type_id', 'participant');
    }

    public function likeCounter()
    {
        return $this->counters()->where('type_id', 'like');
    }

    public function getPlaceCountAttribute()
    {
        return $this->placeCounter ? $this->placeCounter->value : 0;
    }

    public function getPhotoCountAttribute()
    {
        return $this->photoCounter ? $this->photoCounter->value : 0;
    }

    public function getParticipantCountAttribute()
    {
        return $this->participantCounter ? $this->participantCounter->value : 0;
    }

    public function getLikeCountAttribute()
    {
        return ($this->likeCounter ? $this->likeCounter->value : 0) + $this->likesCount;
    }
}
