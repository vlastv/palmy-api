<?php

namespace Palmy;

use Palmy\Observers\TaggableObserve;

trait Taggable
{
    public static function bootTaggable()
    {
        static::observe(TaggableObserve::class);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
