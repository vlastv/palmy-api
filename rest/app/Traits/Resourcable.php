<?php

namespace Palmy\Traits;

use Palmy\Observers\ResourceObserver;
use Palmy\Resource;

trait Resourcable
{
    public function resource()
    {
        return $this->morphOne(Resource::class, 'resource');
    }

    public static function bootResourcable()
    {
        static::observe(new ResourceObserver());
    }

    public function slug()
    {
        return property_exists($this, 'slugKey') ? $this->getAttribute($this->slugKey) : null;
    }

    public function getResourcePathAttribute()
    {
        return $this['resource']['url'];
    }
}
