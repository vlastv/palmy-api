<?php

namespace Palmy\Traits;

use Illuminate\Database\Eloquent\Relations\MorphMany;
use Palmy\Comment;

trait HasComments
{
    public function comments(): MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
