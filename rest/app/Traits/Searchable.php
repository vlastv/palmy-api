<?php

namespace Palmy\Traits;

use Illuminate\Database\Eloquent\Relations\MorphOne;
use Palmy\Observers\SearchableObserve;
use Palmy\Text;

trait Searchable
{
    public static function bootSearchable(): void
    {
        static::observe(new SearchableObserve());
    }

    public function searchText(): MorphOne
    {
        return $this->morphOne(Text::class, 'searchable');
    }
}
