<?php

namespace Palmy\Traits;

use Palmy\Counter;

trait HasCounter
{
    private function counter()
    {
        return $this->morphOne(Counter::class, 'countable');
    }


}
