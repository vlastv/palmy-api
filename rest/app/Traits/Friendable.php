<?php

namespace Palmy\Traits;

use Palmy\Friendship;
use Palmy\User;

trait Friendable
{
    public function friends()
    {
        $this->belongsToMany(User::class)->using(Friendship::class);
    }
}
