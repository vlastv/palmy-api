<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    protected $fillable = ['fact'];

    protected $table = 'feed';

    public static function onDeleting(Model $model)
    {
        Item::where([
            'item_id' => $model->getKey(),
            'item_type' => $model->getMorphClass(),
        ])->delete();
    }

    public function item()
    {
        return $this->morphTo();
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
