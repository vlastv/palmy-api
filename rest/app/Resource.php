<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{
    protected $fillable = ['username'];

    public function resource()
    {
        return $this->morphTo('resource');
    }

    public function scopeLookup(Builder $query, $id)
    {
        if (is_numeric($id)) {
            return $query->whereKey($id);
        }

        $id = ltrim($id, '/');

        return $query->where('username', $id);
    }

    public function getUrlAttribute()
    {
        return '/'.($this['username']?:$this->getKey());
    }
}
