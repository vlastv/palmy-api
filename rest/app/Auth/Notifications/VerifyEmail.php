<?php

namespace Palmy\Auth\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Palmy\Contracts\Auth\NeedVerificationEmail;

class VerifyEmail extends Notification
{
    /**
     * @var
     */
    private $token;

    public function __construct($token)
    {
        $this->token = $token;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail(NeedVerificationEmail $notifiable)
    {
        $pattern = 'https://www.palmy.travel/?externalCommand=actionShowVerifyDialog&externalCommandParams[email]=%email%&externalCommandParams[code]=%code%';

        $url = strtr($pattern, ['%code%' => $this->token, '%email%' => $notifiable->getEmailForVerification()]);

        return (new MailMessage)
            ->line('You are receiving this email because we received a password reset request for your account.')
            ->line('You PIN code: '.$this->token)
            ->action('Verify Email', $url)
            ->line('If you did not request a password reset, no further action is required.');
    }
}
