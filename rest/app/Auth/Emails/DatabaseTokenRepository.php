<?php

namespace Palmy\Auth\Emails;

use Carbon\Carbon;
use Illuminate\Contracts\Hashing\Hasher as HasherContract;
use Illuminate\Database\ConnectionInterface;
use Palmy\Contracts\Auth\NeedVerificationEmail as NeedVerificationEmailContract;

class DatabaseTokenRepository implements TokenRepositoryInterface
{
    /**
     * @var \Illuminate\Database\ConnectionInterface
     */
    private $connection;
    /**
     * @var
     */
    private $table;
    /**
     * @var
     */
    private $hashKey;
    /**
     * @var int
     */
    private $expires;
    /**
     * @var \Illuminate\Contracts\Hashing\Hasher
     */
    private $hasher;

    public function __construct(ConnectionInterface $connection, HasherContract $hasher, $table, $hashKey, $expires = 120)
    {
        $this->connection = $connection;
        $this->hasher = $hasher;
        $this->table = $table;
        $this->hashKey = $hashKey;
        $this->expires = $expires * 60;
    }

    public function create(NeedVerificationEmailContract $user)
    {
        $email = $user->getEmailForVerification();

        $this->deleteExisting($user);

        $token = $this->createNewToken();

        $this->getTable()->insert($this->getPayload($email, $token));

        return $token;
    }

    protected function deleteExisting(NeedVerificationEmailContract $user)
    {
        return $this->getTable()->where('email', $user->getEmailForVerification())->delete();
    }

    public function createNewToken(): string
    {
        $length = 6;

        $pin = '';
        while ($length--) {
            $pin .= (string) random_int(0, 9);
        }

        return $pin;
    }

    public function exists(NeedVerificationEmailContract $user, $token)
    {
        $record = (array) $this->getTable()->where(
            'email',
            $user->getEmailForVerification()
        )->first();

        return $record &&
            ! $this->tokenExpired($record['created_at']) &&
            $this->hasher->check($token, $record['token']);
    }

    protected function tokenExpired($createdAt)
    {
        return Carbon::parse($createdAt)->addSeconds($this->expires)->isPast();
    }

    public function delete(NeedVerificationEmailContract $user)
    {
        $this->deleteExisting($user);
    }

    public function deleteExpired()
    {
        $expiredAt = Carbon::now()->subSeconds($this->expires);

        $this->getTable()->where('created_at', '<', $expiredAt)->delete();
    }

    protected function getPayload($email, $token)
    {
        return ['email' => $email, 'token' => $this->hasher->make($token), 'created_at' => new Carbon];
    }

    public function getConnection()
    {
        return $this->connection;
    }

    protected function getTable()
    {
        return $this->connection->table($this->table);
    }

    public function getHasher()
    {
        return $this->hasher;
    }
}
