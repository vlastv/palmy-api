<?php

namespace Palmy\Auth\Emails;

use Illuminate\Contracts\Auth\UserProvider;
use Illuminate\Support\Arr;

class EmailBroker
{
    /**
     * @var \Palmy\Auth\Emails\TokenRepositoryInterface
     */
    private $tokens;
    /**
     * @var \Illuminate\Contracts\Auth\UserProvider
     */
    private $users;

    public function __construct(TokenRepositoryInterface $tokens, UserProvider $users)
    {
        $this->tokens = $tokens;
        $this->users = $users;
    }

    public function sendVerificationLink(array $credentials)
    {
        $user = $this->getUser($credentials);

        if ($user === null) {
            return 'emails.user';
        }

        $user->sendEmailVerifyNotification(
            $this->tokens->create($user)
        );

        return 'emails.sent';
    }

    /**
     * @param array $credentials
     *
     * @return \Palmy\Contracts\Auth\NeedVerificationEmail
     */
    public function getUser(array $credentials)
    {
        $credentials = Arr::except($credentials, ['token']);

        $user = $this->users->retrieveByCredentials($credentials);

        return $user;
    }
}
