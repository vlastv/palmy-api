<?php

namespace Palmy\Auth\Emails;

use Palmy\Auth\Notifications\VerifyEmail;

trait NeedVerificationEmail
{
    public function getEmailForVerification()
    {
        return $this->email;
    }

    public function sendEmailVerifyNotification($token)
    {
        $this->notify(new VerifyEmail($token));
    }
}
