<?php

namespace Palmy\Auth\Emails;

use Palmy\Contracts\Auth\NeedVerificationEmail as NeedVerificationEmailContract;

interface TokenRepositoryInterface
{
    public function create(NeedVerificationEmailContract $user);
}
