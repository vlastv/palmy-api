<?php

namespace Palmy\Observers;

use Cocur\Slugify\Bridge\Laravel\SlugifyFacade as Slugify;
use Illuminate\Database\Eloquent\Model;

class SluggableObserve
{
    public function saving(Model $model)
    {
        $name = $model['name'];

        $model['slug'] = Slugify::slugify($name);
    }
}
