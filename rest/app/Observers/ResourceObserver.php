<?php

namespace Palmy\Observers;

use Illuminate\Database\Eloquent\Model;

class ResourceObserver
{
    public function saved(Model $model)
    {
        $model['resource'] = $model->resource()->updateOrCreate(
            [],
            [
                'username' => $model->slug()
            ]
        );
    }
}
