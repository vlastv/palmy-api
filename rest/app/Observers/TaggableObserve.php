<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 21.08.17
 * Time: 10:35
 */

namespace Palmy\Observers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Palmy\Tag;

class TaggableObserve
{
    public function saved(Model $model)
    {
        $message = $model['message'];

        $usages = $model->getOwner()->usages();

        if (!empty($message)) {
            $re = '/(?:^|\s)(#[а-яa-z0-9\d-]+)/ui';
            preg_match_all($re, $message, $matches);

            $model->tags()->sync([]);
            foreach ($matches[1] as $name) {
                $tag = Tag::firstOrCreate(['name' => $name]);

                if (!$model->tags->contains($tag->getKey())) {
                    $model->tags()->attach($tag);
                }

                $updated = $usages->updateExistingPivot($tag->getKey(), [
                    'usages' => DB::raw('usages+1'),
                ]);

                if ($updated === 0) {
                    $usages->attach($tag, ['usages' => 1]);
                }
            }
        }

        $model->load('tags');
    }
}
