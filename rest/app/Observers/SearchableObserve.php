<?php

namespace Palmy\Observers;

use Palmy\Contracts\Searchable;

class SearchableObserve
{
    public function saved(Searchable $model)
    {
        $texts = $model->getTextForSearch();

        $filters = $model->getFiltersForSearch();

        $filters = array_map(function (Searchable $filter) {
            return $filter->getPrefix().':'.$filter->getKey();
        }, array_filter($filters));

        $model->searchText()->updateOrCreate([], [
            'text' => implode(' ', array_filter($texts)),
            'filters' => implode(' ', $filters),
        ]);
    }

    public function deleting(Searchable $model)
    {
        $model->searchText()->delete();
    }
}
