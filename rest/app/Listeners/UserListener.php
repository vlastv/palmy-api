<?php

namespace Palmy\Listeners;

use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Canonicalizer\FacebookCanonicalizer;
use Palmy\User;

class UserListener
{
    private $usernameCanonicalizer;
    private $emailCanonicalizer;

    public function __construct()
    {
        $this->usernameCanonicalizer = new FacebookCanonicalizer();
        $this->emailCanonicalizer = new EmailCanonicalizer();
    }

    public function saving(User $user)
    {
        if ($user['username']) {
            $user['username_canonical'] = $this->usernameCanonicalizer->canonicalize($user['username']);
        }

        if ($user['email']) {
            $user['email_canonical'] = $this->emailCanonicalizer->canonicalize($user['email']);
        }
    }
}
