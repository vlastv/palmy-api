<?php

namespace Palmy\Listeners;

use Palmy\Events\Event;

class EventListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Event  $event
     */
    public function handle(Event $event)
    {
        //
    }
}
