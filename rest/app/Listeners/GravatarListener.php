<?php

namespace Palmy\Listeners;

use GuzzleHttp\Client;
use GuzzleHttp\TransferStats;
use Palmy\Events\User\CreateUser;

class GravatarListener
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CreateUser  $event
     */
    public function handle(CreateUser $event)
    {
        $user = $event->getUser();

        if (!$user->needAvatar()) {
            return;
        }

        if ($url = $this->getGravatar($user->email)) {
            $user->avatar = $url;
        }
    }

    private function getGravatar($email)
    {
        $hash = md5(strtolower(trim($email)));
        $uri = 'https://www.gravatar.com/avatar/'.$hash;

        $client = new Client();
        $response = $client->head($uri, [
            'query' => [
                's' => 80,
                'd' => 404,
                'r' => 'g'
            ],
            'http_errors' => false,
            'on_stats' => function (TransferStats $stats) use (&$uri) {
                $uri = (string) $stats->getEffectiveUri();
            }
        ]);

        $code = $response->getStatusCode();

        if ($code < 200 || $code >= 400) {
            return;
        }

        return $uri;
    }
}
