<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 29.09.17
 * Time: 13:51
 */

namespace Palmy;


use Illuminate\Database\Eloquent\Model;

class TripCounter extends Model
{
    public $timestamps = false;

    public function trip()
    {
        return $this->belongsTo(Trip::class);
    }
}
