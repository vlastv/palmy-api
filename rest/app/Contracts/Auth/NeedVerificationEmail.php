<?php

namespace Palmy\Contracts\Auth;

interface NeedVerificationEmail
{
    public function getEmailForVerification();

    public function sendEmailVerifyNotification($token);
}
