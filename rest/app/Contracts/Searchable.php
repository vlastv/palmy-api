<?php

namespace Palmy\Contracts;

use Illuminate\Database\Eloquent\Relations\MorphOne;

interface Searchable
{
    public function searchText(): MorphOne;

    public function getTextForSearch();

    public function getFiltersForSearch();

    public function getPrefix();
}
