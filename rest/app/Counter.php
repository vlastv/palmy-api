<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;

class Counter extends Model
{
    public $timestamps = false;

    public function counterable()
    {
        return $this->morphTo();
    }
}
