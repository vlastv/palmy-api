<?php

namespace Palmy\Hybridauth;

use Hybridauth\Data\Collection;
use Hybridauth\HttpClient\Util;
use Hybridauth\Hybridauth;
use Illuminate\Support\ServiceProvider;

class HybridauthServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register()
    {
        $this->app->singleton(Hybridauth::class, function ($app) {
            $config = config('hybridauth');

            $config['callback'] = Util::getCurrentUrl();

            array_walk($config['providers'], function (&$provider) {
                if (array_key_exists('endpoints', $provider)) {
                    $provider['endpoints'] = new Collection($provider['endpoints']);
                }
            });

            return new Hybridauth($config);
        });
    }

    public function provides()
    {
        return [Hybridauth::class];
    }
}
