<?php

function database_url($urn)
{
    if (empty($urn)) {
        return [];
    }

    $map = [
        'scheme' => 'driver',
        'user' => 'username',
        'path' => 'database',
        'pass' => 'password',
    ];

    $parts = parse_url($urn);

    $config = [];

    if (isset($parts['query'])) {
        parse_str($parts['query'], $config);
    }

    foreach ($parts as $key => $value) {
        $config[isset($map[$key]) ? $map[$key] : $key] = $value;
    }

    if (isset($config['database'])) {
        $config['database'] = trim($config['database'], '/');
    }

    return $config;
}

function array_some(array $array, $callback)
{
    foreach ($array as $k => $v) {
        if ($callback($v, $k)) {
            return true;
        }
    }

    return false;
}
