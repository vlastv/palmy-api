<?php

namespace Palmy;

use Palmy\Observers\SluggableObserve;

trait Sluggable
{
    public static function bootSluggable()
    {
        static::observe(SluggableObserve::class);
    }
}
