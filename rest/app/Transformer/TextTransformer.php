<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 27.08.17
 * Time: 11:37
 */

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Contracts\Searchable;
use Palmy\Place;
use Palmy\Tag;
use Palmy\Text;
use Palmy\Trip;
use Palmy\User;

class TextTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['searchable'];
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(Text $text)
    {
        return [];
    }

    public function includeSearchable(Text $text)
    {
        $searchable = $text->searchable;

        switch (get_class($searchable)) {
            case User::class:
                return $this->item($searchable, new UserTransformer($this->user));
            case Place::class:
                return $this->item($searchable, new PlaceTransformer($this->user));
            case Trip::class:
                return $this->item($searchable, new TripTransformer($this->user));
            case Tag::class:
                return $this->item($searchable, new TagTransformer($this->user));
        }

        return null;
    }
}
