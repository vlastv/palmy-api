<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 09.07.17
 * Time: 16:14
 */

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Place;
use Palmy\User;

class PlaceTransformer extends TransformerAbstract
{
    /**
     * @var \Palmy\User
     */
    private $user;

    protected $defaultIncludes = [
        'cover'
    ];

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(Place $place)
    {
        return [
            'kind' => 'place',
            'id' => $place['scope'].'|'.$place['reference'],
            'name' => $place['name'],
            'location' => [
                'latitude' => (string) $place['latitude'],
                'longitude' => (string) $place['longitude']
            ],
            'viewerIsSubscriber' => $this->user && $this->user->hasSubscribed($place),
            'coverLink' => $place['cover_link'],
        ];
    }

    public function includeCover(Place $place)
    {
        if (null === $cover = $place->cover) {
            return null;
        }

        return $this->item($cover, new MediaTransformer());
    }
}
