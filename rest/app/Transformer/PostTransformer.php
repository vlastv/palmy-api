<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Media;
use Palmy\Post;
use Palmy\User;

class PostTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['photos', 'tags'];

    public function transform(Post $post)
    {
        return [
            'kind' => 'post',
            'id' => ''.$post->id,
            'message' => $post->message,
            'author' => $post->user_id,
            'participants' => $post->participants->map(function (User $user) {
                return $user->id;
            }),
            'place' => $post->place_id ? $post->place->identifier : null,
            'tripId' => $post->trip_id ? ''.$post->trip_id : null,
            'photoLinks' => $post->photos->map(function (Media $media) {
                return $media->url;
            }),
            'publishTime' => $post->created_at->format(DATE_RFC3339),
            'likeCount' => $post->likesCount,
            'viewerHasLiked' => $post->liked,
            'private' => $post->private,
            'resourcePath' => $post->resourcePath,
        ];
    }

    public function includePhotos(Post $post)
    {
        return $this->collection($post->photos, new MediaTransformer());
    }

    public function includeTags(Post $post)
    {
        return $this->collection($post->tags, new TagTransformer());
    }
}
