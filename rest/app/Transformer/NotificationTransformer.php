<?php

namespace Palmy\Transformer;

use Illuminate\Notifications\DatabaseNotification;
use League\Fractal\TransformerAbstract;
use Palmy\Comment;
use Palmy\Notifications\NewComment;
use Palmy\Notifications\NewLike;
use Palmy\Post;
use Palmy\User;

class NotificationTransformer extends TransformerAbstract
{
    /**
     * @var \Palmy\User
     */
    protected $user;

    protected $defaultIncludes = ['data'];

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function transform(DatabaseNotification $notification)
    {
        $payload = [];

        foreach ((array) $notification['data'] as $k => $v) {
            $payload[\camel_case($k)] = $v;
        }

        $parts = explode('\\', $notification['type']);
        $class = end($parts);

        return [
            'kind' => 'notification',
            'id' => $notification['id'],
            'type' => $class,
            'payload' => $payload,
            'readByMe' => $notification->read(),
            'readByMeTime' => $notification['read_at'] ? $notification['read_at']->format(DATE_RFC3339) : null,
        ];
    }

    public function includeData(DatabaseNotification $notification)
    {

        $type = $notification['type'];
        $data = $notification['data'];

        if ($type === NewComment::class) {
            return $this->item($data, new NewCommentNotificationTransformer($this->user));
        }

        if ($type === NewLike::class) {
            return $this->item($data, new NewLikeNotificationTransformer($this->user));
        }

        return [];
    }
}
