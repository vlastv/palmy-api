<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Item;
use Palmy\Post;
use Palmy\Trip;

class StatusTransformer extends TransformerAbstract
{
    protected $availableIncludes = ['post'];

    public function transform(Item $item)
    {
        $data = [
            'kind' => 'item',
            'hitTime' => $item->created_at->format(DATE_RFC3339),
            'fact' => $item->fact,
        ];

        if ($item->item_type === Post::class) {
            $data['postId'] = ''.$item->item_id;
        }

        if ($item->item_type === Trip::class) {
            $data['tripId'] = ''.$item->item_id;
        }

        return $data;
    }

    public function includePost(Item $status)
    {
        $item = $status->item;

        switch (true) {
            case $item instanceof Post:
                return $this->item($item, new PostTransformer());

            case $item instanceof Trip:
                return $this->item($item, new TripTransformer());
        }

        return null;
    }
}
