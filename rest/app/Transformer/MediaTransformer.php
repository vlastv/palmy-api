<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Media;
use Palmy\User;

class MediaTransformer extends TransformerAbstract
{
    /**
     * @var \Palmy\User
     */
    private $user;
    /**
     * @var null
     */
    private $url;

    public function __construct(User $user = null, $url = null)
    {
        $this->user = $user;
        $this->url = $url;
    }

    public function transform(Media $media)
    {
        return [
            'kind' => 'media',
            'id' => $media->getKey(),
            'ownedByMe' => $this->user ? $media['owner']->is($this->user) : false,
            'uploadUrl' => $this->url,
            'viewUrl' => $media['url']
        ];
    }
}
