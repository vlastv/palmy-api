<?php

namespace Palmy\Transformer;

use Illuminate\Notifications\DatabaseNotification;
use League\Fractal\TransformerAbstract;
use Palmy\Comment;
use Palmy\Post;
use Palmy\Trip;
use Palmy\User;

class NewCommentNotificationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['comment', 'subject'];
    /**
     * @var \Palmy\User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function transform($data)
    {
        return [];
    }

    public function includeComment($data)
    {
        return $this->item($this->getComment($data), new CommentTransformer($this->user));
    }

    public function includeSubject($data)
    {
        $commentable = $this->getComment($data)->commentable;

        switch ($commentable->getMorphClass()) {
            case Post::class:
                $transformer = new PostTransformer($this->user);
                break;
            case Trip::class:
                $transformer = new TripTransformer($this->user);
                break;
        }

        return $this->item($commentable, $transformer);
    }

    private function getComment($data)
    {
        return Comment::find($data['comment_id']);
    }
}
