<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Trip;
use Palmy\User;

class TripTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cover'
    ];

    /**
     * @var \Palmy\User
     */
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(Trip $trip)
    {
        return array_filter([
            'kind' => 'trip',
            'id' => ''.$trip['id'],
            'name' => $trip['name'],
            'startDate' => $trip['start'] ? $trip['start']->format('Y-m-d') : null,
            'endDate' => $trip['end'] ? $trip['end']->format('Y-m-d') : null,
//            'videoCount' => $trip['videos_count'] ?: 0,
            'photoCount' => $trip['photo_count'],
            'participantCount' => $trip['participant_count'],
            'placeCount' => $trip['place_count'],
            'likesCount' => $trip['like_count'],
            'coverLink' => $trip['cover_url'],
            'ownedByMe' => $trip['user'] && $this->user ? $trip['user']->is($this->user) : false,
            'resourcePath' => $trip['resourcePath'],
            'private' => $trip['private'],
            'participants' => $trip->participants->map(function ($user) {
                return $user->id;
            }),
            'author' => $trip->user->id,
            'place' => $trip->place ? $trip->place->identifier : null,
            'likeCount' => $trip->likesCount,
            'viewerHasLiked' => $trip->liked,
            'viewerIsSubscriber' => $this->user && $this->user->hasSubscribed($trip),

        ], function ($v) {
            return $v !== null;
        });
    }

    public function includeCover(Trip $trip)
    {
        $cover = $trip->cover;

        return $cover ? $this->item($cover, new MediaTransformer()) : null;
    }
}
