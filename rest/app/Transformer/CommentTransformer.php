<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Comment;
use Palmy\User;

class CommentTransformer extends TransformerAbstract
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function transform(Comment $comment)
    {
        $owned = $comment->isOwnedBy($this->user);

        return [
            'id' => ''.$comment->id,
            'createdTime' => $comment->created_at->format(\DateTime::ATOM),
            'message' => $comment->message,
            'authorId' => $comment->author_id,
            'capabilities' => [
                'canDelete' => $owned,
                'canEdit' => $owned,
            ],
            'ownedByMe' => $owned,
        ];
    }
}
