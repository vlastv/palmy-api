<?php

namespace Palmy\Transformer;

use Cog\Likeable\Models\Like;
use League\Fractal\TransformerAbstract;
use Palmy\Post;
use Palmy\Trip;
use Palmy\User;

class NewLikeNotificationTransformer extends TransformerAbstract
{
    protected $defaultIncludes = ['subject'];
    /**
     * @var \Palmy\User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function transform($data)
    {
        return [];
    }

    public function includeSubject($data)
    {
        $likable = $this->getLike($data)->likeable;

        switch ($likable->getMorphClass()) {
            case Post::class:
                $transformer = new PostTransformer($this->user);
                break;
            case Trip::class:
                $transformer = new TripTransformer($this->user);
                break;
        }

        return $this->item($likable, $transformer);
    }

    private function getLike($data)
    {
        return Like::find($data['like_id']);
    }
}
