<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 09.07.17
 * Time: 16:14
 */

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Tag;
use Palmy\User;

class TagTransformer extends TransformerAbstract
{
    /**
     * @var \Palmy\User
     */
    private $user;

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(Tag $tag)
    {
        return [
            'kind' => 'hashtag',
            'id' => $tag->id,
            'name' => $tag->name,
            'slug' => $tag->slug,
            'viewerIsSubscriber' => $this->user && $this->user->hasSubscribed($tag),
        ];
    }
}
