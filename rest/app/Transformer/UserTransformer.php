<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Identity;
use Palmy\User;

class UserTransformer extends TransformerAbstract
{
    const RESOURCE_KEY = 'users';
    /**
     * @var \Palmy\User
     */
    private $user;

    protected $defaultIncludes = [
        'cover'
    ];

    public function __construct(User $user = null)
    {
        $this->user = $user;
    }

    public function transform(User $user)
    {
        return array_filter([
            'kind' => 'user',
            'id' => $user['id'],
            'username' => $user['username'],
            'firstName' => $user['first_name'],
            'lastName' => $user['last_name'],
            'email' => $user['email'],
            'birthdayDate' => $user['birthday'] ? $user['birthday']->format('Y-m-d') : null,
            'phone' => [
                'number' => $user['phone_number'],
            ],
            'country' => $user['country'],
            'city' => $user['city'],
            'me' => $this->user ? $user->is($this->user) : false,
            'resourcePath' => $user['resource']['url'],
            'gender' => $user['gender'],
            'identities' => $user['identities']->map(function (Identity $identity) {
                return [
                    'identifier' => $identity['identifier'],
                    'provider' => $identity['provider'],
                    'avatarLink' => $identity['avatar'],
                    'name' => $identity['name']
                ];
            }),
            'avatarLink' => $user['avatar_link'],
            'avatar' => $user['avatar_link'],
            'coverLink' => $user['cover_link'],
            'viewerIsFollower' => $this->user ? $user->isFollowedBy($this->user) : false,
            'followerCount' => $user->followers()->count(),
            'followingCount' => $user->followings()->count(),
            'tripCount' => $user->trips()->count(),
            'postCount' => $user->posts()->count(),
            'own' => $user->password !== null,
        ], function ($v) {
            return $v !== null;
        });
    }

    public function includeCover(User $user)
    {
        if (null === $cover = $user->cover) {
            return null;
        }

        return $this->item($cover, new MediaTransformer());
    }
}
