<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\City;
use Palmy\Comment;
use Palmy\Country;
use Palmy\User;

class CityTransformer extends TransformerAbstract
{
    public function transform(City $city)
    {
        return [
            'kind' => 'city',
            'id' => ''.$city->id,
            'name' => $city->name,
            'regionName' => $city->region,
            'areaName' => $city->area,
        ];
    }
}
