<?php

namespace Palmy\Transformer;

use League\Fractal\TransformerAbstract;
use Palmy\Comment;
use Palmy\Country;
use Palmy\User;

class CountryTransformer extends TransformerAbstract
{
    public function transform(Country $country)
    {
        return [
            'kind' => 'country',
            'id' => ''.$country->id,
            'name' => $country->name,
        ];
    }
}
