<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    public $timestamps = false;
    protected $fillable = ['text', 'filters'];

    public function searchable()
    {
        return $this->morphTo();
    }

    public function scopeSearch(Builder $builder, $query)
    {
        return $builder->whereRaw('MATCH(text, filters) AGAINST(? IN BOOLEAN MODE)', [$query]);
    }
}
