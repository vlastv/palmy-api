<?php

namespace Palmy;

use Cog\Ownership\Contracts\HasOwner as HasOwnerContract;
use Cog\Ownership\Traits\HasOwner;
use Illuminate\Database\Eloquent\Model;
use Monolog\Handler\NewRelicHandler;
use Palmy\Notifications\NewComment;

class Comment extends Model implements HasOwnerContract
{
    use HasOwner;

    protected $fillable = ['message'];

    protected $ownerForeignKey = 'author_id';

    protected static function boot()
    {
        parent::boot();

        static::created(function(Comment $comment) {
            $commentable = $comment->commentable;
            $owner = $commentable->getOwner();
            if ($commentable instanceof HasOwnerContract && $comment->isNotOwnedBy($owner)) {
                $owner->notify(new NewComment($comment));
            }
        });
    }

    public function commentable()
    {
        return $this->morphTo();
    }
}
