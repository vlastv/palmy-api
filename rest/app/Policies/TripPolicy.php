<?php

namespace Palmy\Policies;

use Cog\Ownership\Contracts\CanBeOwner as CanBeOwnerContract;
use Illuminate\Auth\Access\HandlesAuthorization;
use Palmy\Trip;
use Palmy\User;

class TripPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the trip.
     *
     * @param  \Palmy\User  $user
     * @param  \Palmy\Trip  $trip
     *
     * @return mixed
     */
    public function view(User $user, Trip $trip)
    {
        //
    }

    /**
     * Determine whether the user can create trips.
     *
     * @param  \Palmy\User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the trip.
     *
     * @param  \Palmy\User  $user
     * @param  \Palmy\Trip  $trip
     *
     * @return mixed
     */
    public function update(User $user, Trip $trip)
    {
        return $this->isOwned($user, $trip);
    }

    /**
     * Determine whether the user can delete the trip.
     *
     * @param  \Palmy\User  $user
     * @param  \Palmy\Trip  $trip
     *
     * @return mixed
     */
    public function delete(User $user, Trip $trip)
    {
        return $this->isOwned($user, $trip);
    }

    private function isOwned(CanBeOwnerContract $user, Trip $trip)
    {
        return $trip->isOwnedBy($user);
    }
}
