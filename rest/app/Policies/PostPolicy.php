<?php

namespace Palmy\Policies;

use Illuminate\Auth\Access\HandlesAuthorization;
use Palmy\Post;
use Palmy\User;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \Palmy\User  $user
     * @param  \Palmy\Post  $post
     *
     * @return mixed
     */
    public function view(User $user, Post $post)
    {
        //
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \Palmy\User  $user
     *
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }

    public function update(User $user, Post $post): bool
    {
        return $this->isOwned($user, $post);
    }

    public function delete(User $user, Post $post): bool
    {
        return $this->isOwned($user, $post);
    }

    private function isOwned(User $user, Post $post): bool
    {
        return $post->isOwnedBy($user);
    }
}
