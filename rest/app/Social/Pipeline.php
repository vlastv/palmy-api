<?php

namespace Palmy\Social;

use Hybridauth\User\Profile;
use Palmy\Identity;
use Palmy\User;

class Pipeline
{
    /**
     * @param $provider
     * @param \Hybridauth\User\Profile $profile
     * @param \Palmy\User|null $user
     *
     * @return \Illuminate\Foundation\Auth\User
     */
    public function process($provider, Profile $profile, User $user = null)
    {
        //socialDetails

        //social_uid

        //auth_allowed

        //social_user
        /** @var Identity $identity */
        $identity = Identity::updateOrCreate(
            [
                'provider' => $provider,
                'identifier' => $profile->identifier,
            ],
            [
                'raw' => (array) $profile,
                'name' => $profile->displayName,
                'avatar' => $profile->photoURL,
            ]
        );

        if ($user && $identity->user !== $user) {
            throw new \RuntimeException();
        } elseif ($user === null) {
            $user = $identity->user;
        }

        //get_username
        $userCount = 0;
        //associate_by_email
        if ($user === null && $profile->emailVerified) {
            $users = User::where('email', $profile->emailVerified)->get();
            $userCount = count($users);

            if ($userCount === 1) {
                $user = $users->first();
            }
        }

        //create_user
        if ($user === null) {
            $user = User::create([
                'email' => $userCount === 0 ? $profile->emailVerified : null
            ]);
        }

        //associate_user
        $identity->user()->associate($user);

        $map = [
            'city' => 'city',
            'country' => 'country',
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'gender' => 'gender',
            'photoURL' => 'avatar',
            'phone' => 'phone_number'
        ];

        foreach ($map as $from => $to) {
            if ($profile->$from && !$user->$to) {
                $user->$to = $profile->$from;
            }
        }

        if (!$user->birthday && $profile->birthDay && $profile->birthMonth && $profile->birthYear) {
            $birthday = \DateTime::createFromFormat('Y-n-j', implode('-', [
                $profile->birthYear,
                $profile->birthMonth,
                $profile->birthDay
            ]));

            if ($birthday !== false) {
                $user->birthday = $birthday;
            }
        }

        $identity->save();
        $user->save();

        return $user;
    }
}
