<?php

namespace Palmy;

use Cog\Ownership\Contracts\CanBeOwner as CanBeOwnerContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanSubscribe;
use Palmy\Auth\Emails\NeedVerificationEmail;
use Palmy\Contracts\Auth\NeedVerificationEmail as NeedVerificationEmailContract;
use Palmy\Contracts\Searchable as SearchableContract;
use Palmy\Events\User\CreateUser;
use Palmy\Traits\Friendable;
use Palmy\Traits\Resourcable;
use Palmy\Traits\Searchable;
use Palmy\Notifications\ResetPassword as ResetPasswordNotification;

class User extends Authenticatable implements NeedVerificationEmailContract, CanBeOwnerContract, SearchableContract
{
    use HasApiTokens, Notifiable, Friendable, Resourcable, NeedVerificationEmail;
    use CanFollow, CanBeFollowed;
    use CanSubscribe;
    use Searchable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'first_name',
        'last_name',
        'avatar',
        'username',
        'gender',
        'phone_number',
        'birthday',
        'country',
        'city',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
        'username_canonical', 'email_canonical'
    ];

    protected $events = [
        'creating' => CreateUser::class
    ];

    protected $casts = [
        'birthday' => 'date',
    ];

    protected $slugKey = 'username';

    public function needAvatar()
    {
        return null !== $this->email && null === $this->avatar;
    }

    public function avatar()
    {
        return $this->belongsTo(Media::class);
    }

    public function cover()
    {
        return $this->belongsTo(Media::class);
    }

    public function scopeSearch(Builder $query, $value)
    {
        $like = '%' . $value . '%';

        return $query
            ->where('first_name', 'LIKE', $like)
            ->orWhere('last_name', 'LIKE', $like)
            ->orWhere('username', 'LIKE', $like)
            ->orWhere('username_canonical', 'LIKE', $like)
            ;
    }

    public function identities()
    {
        return $this->hasMany(Identity::class);
    }

    public function getAvatarLinkAttribute()
    {
        $media = $this->getRelationValue('avatar');

        return $media ? $media['url'] : $this->getAttributeValue('avatar');
    }

    public function getCoverLinkAttribute()
    {
        $media = $this->getRelationValue('cover');

        return $media ? $media['url'] : null;
    }

    public function feed()
    {
        return $this->hasMany(Item::class)->latest();
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    public function usages()
    {
        return $this->belongsToMany(Tag::class, 'used_tags')
            ->withPivot('usages');
    }

    public function getTextForSearch()
    {
        return [
            $this->username,
            $this->last_name,
            $this->first_name,
        ];
    }

    public function getFiltersForSearch()
    {
        return [];
    }

    public function getPrefix()
    {
        return 'user';
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function medias()
    {
        return $this->hasMany(Media::class);
    }
}
