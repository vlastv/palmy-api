<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Media extends Model
{
    protected $fillable = [
        'name', 'mime_type', 'user_id',
        'provider_name', 'provider_reference',
        'context'
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, null, null, 'user');
    }

    public function getUrlAttribute()
    {
        return Storage::disk($this->getAttribute('provider_name'))->url($this->getAttribute('provider_reference'));
    }
}
