<?php

namespace Palmy\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Palmy\User;

class FriendRegistered extends Notification
{
    use Queueable;
    /**
     * @var \Palmy\User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_id' => $this->user['id']
        ];
    }
}
