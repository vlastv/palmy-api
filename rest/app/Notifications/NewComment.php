<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 08.10.17
 * Time: 11:58
 */

namespace Palmy\Notifications;


use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Palmy\Comment;

class NewComment extends Notification
{
    use Queueable;
    /**
     * @var \Palmy\Comment
     */
    private $comment;

    public function __construct(Comment $comment)
    {
        $this->comment = $comment;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        $commentable = $this->comment->commentable;
        return [
            'comment_id' => $this->comment->getKey(),
            'commentable' => [
                'id' => $commentable->getKey(),
                'type' => $commentable->getMorphClass(),
            ]
        ];
    }
}
