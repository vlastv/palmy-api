<?php

namespace Palmy\Notifications;

use Cog\Likeable\Models\Like;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class NewLike extends Notification
{
    use Queueable;

    /**
     * @var \Cog\Likeable\Models\Like
     */
    private $like;

    public function __construct(Like $like)
    {
        $this->like = $like;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'like_id' => $this->like->getKey(),
        ];
    }
}
