<?php

namespace Palmy\Repositories;

use GuzzleHttp\Client;
use Palmy\Place;

class PlaceRepository
{
    public function find($id)
    {
        list($scope, $reference) = explode('|', $id, 2);

        $place = Place::where([
            'scope' => $scope,
            'reference' => $reference
        ])->first();

        if ($place !== null) {
            return $place;
        }

        $result = $this->proxy($reference);

        if ($result['status'] !== 'OK') {
            return abort(404);
        }

        $place = Place::create([
            'scope' => $scope,
            'reference' => $reference,
            'name' => $result['result']['name'],
            'latitude' => $result['result']['geometry']['location']['lat'],
            'longitude' => $result['result']['geometry']['location']['lng'],
        ]);

        return $place;
    }

    private function proxy($id)
    {
        $client = new Client();

        $url = 'https://maps.googleapis.com/maps/api/place/details/json'
            . '?' . http_build_query([
                'key' => config('services.google.key'),
                'placeid' => $id,
                'language' => 'ru'
            ]);

        $response = $client->get($url);

        return \GuzzleHttp\json_decode($response->getBody(), true);
    }
}
