<?php

namespace Palmy;

use Illuminate\Database\Eloquent\Model;
use Palmy\Contracts\Searchable as SearchableContract;
use Palmy\Traits\Searchable;

class Place extends Model implements SearchableContract
{
    use Searchable;

    protected $fillable = ['scope', 'name', 'longitude', 'latitude', 'reference'];

    public function cover()
    {
        return $this->belongsTo(Media::class);
    }

    protected static function boot()
    {
        static::saving(function (Place $place) {
            $place->identifier = $place->scope.'|'.$place->reference;
        });

        parent::boot();
    }

    public function jsonSerialize()
    {
        return array_merge(parent::jsonSerialize(), [
            'id' => $this->identifier,
        ]);
    }

    public function getTextForSearch()
    {
        return [
            $this->name,
        ];
    }

    public function getFiltersForSearch()
    {
        return [];
    }

    public function getPrefix()
    {
        return 'place';
    }

    public function getRouteKeyName()
    {
        return 'identifier';
    }

    public function getCoverLinkAttribute()
    {
        $media = $this->getRelationValue('cover');

        return $media ? $media['url'] : null;
    }
}
