<?php

namespace Palmy\Canonicalizer;

class ChainCanonicalizer implements CanonicalizerInterface
{
    /**
     * @var array
     */
    private $children;

    public function __construct(array $children = [])
    {
        $this->children = $children;
    }

    public function canonicalize($string)
    {
        $result = $string;

        foreach ($this->children as $child) {
            $result = $child->canonicalize($result);
        }

        return $result;
    }
}
