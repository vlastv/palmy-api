<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 29.04.17
 * Time: 1:38
 */

namespace Palmy\Canonicalizer;

interface CanonicalizerInterface
{
    public function canonicalize($string);
}
