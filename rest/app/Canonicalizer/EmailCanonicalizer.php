<?php

namespace Palmy\Canonicalizer;

class EmailCanonicalizer extends Canonicalizer
{
    public function canonicalize($string)
    {
        $result = parent::canonicalize($string);

        if (null === $result) {
            return;
        }

        list($user, $host) = explode('@', $result, 2);

        getmxrr($host, $mxHosts);

        if ($this->isGoogle($mxHosts)) {
            $user = str_replace('.', '', $user);

            if (false !== $pos = strpos($user, '+')) {
                $user = substr($user, 0, $pos);
            }
        }

        return $user.'@'.$host;
    }

    private function isGoogle($hosts)
    {
        return $this->isMxOnDomain($hosts, '.google.com');
    }

    private function isMxOnDomain(array $hosts, $domain)
    {
        return array_some($hosts, function ($host) use ($domain) {
            return false !== strpos($host, $domain);
        });
    }
}
