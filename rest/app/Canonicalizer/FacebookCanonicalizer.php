<?php

namespace Palmy\Canonicalizer;

class FacebookCanonicalizer extends Canonicalizer
{
    public function canonicalize($string)
    {
        if ($string === null) {
            return null;
        }

        $result = parent::canonicalize($string);

        $result = str_replace('.', '', $result);

        return $result;
    }
}
