<?php

namespace Palmy\Serializer;

use League\Fractal\Serializer\ArraySerializer;

class ApiSerializer extends ArraySerializer
{
    public function meta(array $meta)
    {
        return $meta;
    }
}
