<?php

namespace Palmy;

use Illuminate\Notifications\Notifiable;
use Palmy\Auth\Emails\NeedVerificationEmail;
use Palmy\Contracts\Auth\NeedVerificationEmail as NeedVerificationEmailContract;

class VerificationEmail implements NeedVerificationEmailContract
{
    use NeedVerificationEmail;
    use Notifiable;

    /**
     * @var string
     */
    private $email;

    public function __construct(string $email)
    {
        $this->email = $email;
    }
}
