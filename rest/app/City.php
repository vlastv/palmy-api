<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 02.10.17
 * Time: 3:49
 */

namespace Palmy;


use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
