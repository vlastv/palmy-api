<?php

namespace Palmy\Providers;

use Cog\Likeable\Models\Like;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Palmy\Events;
use Palmy\Listeners;
use Palmy\Notifications\NewLike;
use Palmy\User;
use Cog\Ownership\Contracts\HasOwner as HasOwnerContract;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Events\Event::class => [
            Listeners\EventListener::class,
        ],
        Events\User\CreateUser::class => [
            Listeners\GravatarListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     */
    public function boot()
    {
        parent::boot();

        User::observe(Listeners\UserListener::class);

        Like::created(function (Like $like) {
            $likable = $like->likeable;
            $owner = $likable->getOwner();
            if ($likable instanceof HasOwnerContract && $like['user_id'] !== $owner->getKey()) {
                $owner->notify(new NewLike($like));
            }
        });
    }
}
