<?php

namespace Palmy\Providers;

use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use Palmy\Serializer\ApiSerializer;

class FractalServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->singleton(Manager::class, function () {
            $manager = new Manager();
            $manager->setSerializer(new ApiSerializer());

            return $manager;
        });
    }

    public function boot()
    {
        response()->macro('item', function ($item, $transformer, $status = 200, array $headers = []) {
            $resource = new Item($item, $transformer);

            return response()->resource($resource, $status, $headers);
        });

        response()->macro('collection', function ($collection, $transformer, $status = 200, array $headers = []) {
            $resourceKey = defined(get_class($transformer).'::RESOURCE_KEY') ? $transformer::RESOURCE_KEY : 'data';
            $resource = new Collection($collection, $transformer, $resourceKey);

            return response()->resource($resource, $status, $headers);
        });

        response()->macro('resource', function ($resource, $status = 200, $headers = []) {
            return response()->json(
                app(Manager::class)->createData($resource)->toArray(),
                $status,
                $headers
            );
        });
    }
}
