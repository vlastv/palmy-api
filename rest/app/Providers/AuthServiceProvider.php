<?php

namespace Palmy\Providers;

use Hybridauth\Hybridauth;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Database\ConnectionInterface;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Laravel\Passport\Passport;
use Laravel\Passport\RouteRegistrar;
use League\OAuth2\Server\AuthorizationServer;
use Palmy\Auth\Emails\DatabaseTokenRepository;
use Palmy\Auth\Emails\EmailBroker;
use Palmy\OAuth2\AccessTokenGrant;
use Palmy\OAuth2\OneTimeTokenGrantType;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'Palmy\Post' => 'Palmy\Policies\PostPolicy',
        \Palmy\Trip::class => \Palmy\Policies\TripPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function (RouteRegistrar $router) {
            $router->forAccessTokens();
            $router->forTransientTokens();
        });
    }

    public function register()
    {
        $key = config('app.key');

        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        $this->app->singleton(DatabaseTokenRepository::class, function () use ($key) {
            return new DatabaseTokenRepository(
                $this->app->make(ConnectionInterface::class),
                $this->app->make(Hasher::class),
                'email_verifications',
                $key,
                120
            );
        });

        $this->app->singleton(EmailBroker::class, function () {
            return new EmailBroker(
                $this->app->make(DatabaseTokenRepository::class),
                Auth::createUserProvider('users')
            );
        });

        $this->app->extend(AuthorizationServer::class, function (AuthorizationServer $server) {
            $server->enableGrantType($this->makeAccessTokenGrant(), Passport::tokensExpireIn());
            $server->enableGrantType($this->makeOneTimeTokenGrantType(), Passport::tokensExpireIn());

            return $server;
        });
    }

    private function makeOneTimeTokenGrantType()
    {
        $grantType = new OneTimeTokenGrantType(
            $this->app->make(DatabaseTokenRepository::class),
            $this->app->make(RefreshTokenRepository::class),
            $this->app->make(EmailBroker::class)
        );

        $grantType->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grantType;
    }

    private function makeAccessTokenGrant()
    {
        $grant = new AccessTokenGrant(
            $this->app->make(Hybridauth::class),
            $this->app->make(RefreshTokenRepository::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }
}
