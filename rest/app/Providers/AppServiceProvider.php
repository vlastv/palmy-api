<?php

namespace Palmy\Providers;

use Illuminate\Database\Events\QueryExecuted;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        //        DB::listen(function (QueryExecuted $query) {
//            Log::debug($query->sql, [$query->bindings, $query->time]);
//        });

        Validator::extend('user_password', function($attribute, $value, $parameters, $validator) {
            $user = Auth::user();

            return Hash::check($value, $user->password);
        });
    }
}
