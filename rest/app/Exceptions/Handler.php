<?php

namespace Palmy\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception $exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Exception $exception
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->isJson()) {
            if ($exception instanceof HttpException) {
                return response()->json([
                    'error' => [
                        'code' => $exception->getStatusCode(),
                        'message' => $exception->getMessage(),
                    ]
                ], $exception->getStatusCode());
            }

            if ($exception instanceof ModelNotFoundException) {
                return response()->json([
                    'error' => [
                        'code' => 404,
                        'message' => $exception->getMessage(),
                    ]
                ], 404);
            }
        }

        return parent::render($request, $exception);
    }

    protected function convertValidationExceptionToResponse(ValidationException $e, $request)
    {
        $errors = [];

        foreach ($e->validator->getMessageBag()->messages() as $field => $constraints) {
            foreach ($constraints as $constraint) {
                $errors[] = [
                    'reason' => $constraint,
                    'location' => $field,
                    'locationType' => 'body'
                ];
            }
        }

        return response()->json([
            'error' => [
                'errors' => $errors,
                'code' => 422,
                'message' => Response::$statusTexts[422]
            ]
        ], 422);
    }

    /**
     * Convert an authentication exception into an unauthenticated response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Illuminate\Auth\AuthenticationException $exception
     *
     * @return \Illuminate\Http\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['error' => 'Unauthenticated.'], 401);
    }
}
