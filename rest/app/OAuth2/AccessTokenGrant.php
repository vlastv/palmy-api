<?php

namespace Palmy\OAuth2;

use Hybridauth\Adapter\AdapterInterface;
use Hybridauth\Exception\UnexpectedValueException;
use Hybridauth\Hybridauth;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Palmy\UserProvider;
use Psr\Http\Message\ServerRequestInterface;

class AccessTokenGrant extends AbstractGrant
{
    /**
     * @var \Hybridauth\Hybridauth
     */
    private $hybridauth;

    public function __construct(Hybridauth $hybridauth, RefreshTokenRepositoryInterface $refreshTokenRepository)
    {
        $this->setRefreshTokenRepository($refreshTokenRepository);
        $this->hybridauth = $hybridauth;
    }

    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        \DateInterval $accessTokenTTL
    ) {
        $client = $this->validateClient($request);

        $user = $this->validateAccessToken($request);

        // Issue and persist access token
        $accessToken = $this->issueAccessToken(
            $accessTokenTTL,
            $client,
            $user->getIdentifier()
        );

        $refreshToken = $this->issueRefreshToken($accessToken);

        // Inject access token into response type
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    /**
     * @param \Psr\Http\Message\ServerRequestInterface $request
     *
     * @throws \League\OAuth2\Server\Exception\OAuthServerException
     *
     * @return \Palmy\User
     */
    private function validateAccessToken(ServerRequestInterface $request)
    {
        $provider = $this->getRequestParameter('provider', $request);
        if (null === $provider) {
            throw OAuthServerException::invalidRequest('provider');
        }

        $accessToken = $this->getRequestParameter('access_token', $request);
        if (null === $accessToken) {
            throw OAuthServerException::invalidRequest('access_token');
        }

        $adapter = $this->hybridauth->getAdapter(ucfirst(strtolower($provider)));
        $adapter->setAccessToken([
            'access_token' => $accessToken
        ]);

        $profile = $this->getProfile($adapter);

        $userProvider = new UserProvider();
        $user = $userProvider->loadUserByProfile($profile, $provider);

        if (!$user instanceof UserEntityInterface) {
            throw OAuthServerException::invalidCredentials();
        }

        return $user;
    }

    public function getIdentifier()
    {
        return 'urn:app:params:oauth:grant-type:access_token';
    }

    private function getProfile(AdapterInterface $adapter)
    {
        try {
            $profile = $adapter->getUserProfile();
        } catch (UnexpectedValueException $exception) {
            throw OAuthServerException::invalidRequest('access_token');
        }

        return $profile;
    }
}
