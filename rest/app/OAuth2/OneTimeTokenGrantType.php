<?php

namespace Palmy\OAuth2;

use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Palmy\Auth\Emails\DatabaseTokenRepository;
use Palmy\Auth\Emails\EmailBroker;
use Psr\Http\Message\ServerRequestInterface;

final class OneTimeTokenGrantType extends AbstractGrant
{
    private $oneTimeTokenRepository;
    /**
     * @var \Palmy\Auth\Emails\EmailBroker
     */
    private $broker;

    public function __construct(
        DatabaseTokenRepository $oneTimeTokenRepository,
        RefreshTokenRepositoryInterface $refreshTokenRepository,
        EmailBroker $broker
    ) {
        $this->oneTimeTokenRepository = $oneTimeTokenRepository;
        $this->setRefreshTokenRepository($refreshTokenRepository);
        $this->broker = $broker;
    }

    public function getIdentifier()
    {
        return 'urn:app:params:oauth:grant-type:onetime';
    }

    public function respondToAccessTokenRequest(
        ServerRequestInterface $request,
        ResponseTypeInterface $responseType,
        \DateInterval $accessTokenTTL
    ) {
        $client = $this->validateClient($request);

        $user = $this->validateToken($request);

        $accessToken = $this->issueAccessToken(
            $accessTokenTTL,
            $client,
            $user->identifier
        );

        $refreshToken = $this->issueRefreshToken($accessToken);

        // Inject access token into response type
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    private function validateToken(ServerRequestInterface $request)
    {
        $username = $this->getRequestParameter('username', $request);
        if (null === $username) {
            throw OAuthServerException::invalidRequest('username');
        }

        $password = $this->getRequestParameter('password', $request);
        if (null === $password) {
            throw OAuthServerException::invalidRequest('password');
        }

        $user = $this->broker->getUser([
            'email' => $username
        ]);

        if (!$user) {
            throw OAuthServerException::invalidCredentials();
        }


        if ($this->oneTimeTokenRepository->exists($user, $password)) {
            $this->oneTimeTokenRepository->delete($user);

            return $user;
        }

        throw OAuthServerException::invalidCredentials();
    }
}
