<?php

namespace Palmy\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class LoadGeoFromVK extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'load:geo';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $raw = $this->do('https://api.vk.com/api.php?oauth=1&method=database.getCountries&v=5.5&need_all=1&count=1000');
        $json = json_decode($raw);

        $table = DB::table('countries');
        foreach($json->response->items as $item) {
            DB::beginTransaction();
            $table->insert([
                'name' => $item->title,
            ]);

            $this->loadRegion($item->id);
            DB::commit();
        }
    }

    private function loadRegion($countryId)
    {
        $raw = $this->do('https://api.vk.com/api.php?oauth=1&method=database.getRegions&v=5.5&need_all=1&offset=0&count=1000&country_id='.$countryId);
        $json = json_decode($raw);

        $table = DB::table('regions');
        foreach($json->response->items as $item) {
            $table->insert([
                'country_id' => $countryId,
                'name' => $item->title,
            ]);

            $this->loadCity($countryId, $item->id);
        }
    }

    private function loadCity($countryId, $regionId)
    {
        $raw = $this->do("https://api.vk.com/api.php?oauth=1&method=database.getCities&v=5.5&country_id={$countryId}&region_id={$regionId}");
        $json = json_decode($raw);

        $table = DB::table('cities');
        foreach($json->response->items as $item) {
            $table->insert([
                'country_id' => $countryId,
                'region_id' => $regionId,
                'name' => $item->title,
            ]);
        }
    }

    private function do($url)
    {
        $headerOptions = array(
            'http' => array(
                'method' => 'GET',
                'header' => "Accept-language: en\r\n" .
                    "Cookie: remixlang=0\r\n"
            )
        );
        $streamContext = stream_context_create($headerOptions);

        return file_get_contents($url, false, $streamContext);
    }
}
