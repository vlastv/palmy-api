<?php

namespace Palmy\Http\Controllers\Place;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Place;
use Palmy\Transformer\PlaceTransformer;

class Index extends Controller
{
    public function __invoke(Request $request)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = Place::query()
            ->take($limit+1)
            ->skip($offset)
        ;

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($posts->slice(0, $limit), new PlaceTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
