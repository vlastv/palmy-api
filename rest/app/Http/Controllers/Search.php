<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Place;
use Palmy\Post;
use Palmy\Tag;
use Palmy\Text;
use Palmy\Transformer\TextTransformer;
use Palmy\Trip;
use Palmy\User;

class Search
{
    public function __invoke(Request $request)
    {
        $text = $request->get('text');
        $type = $request->get('corpus');

        $words = array_filter(array_map('trim', explode(' ', $text)));

        $text = implode('* ', $words).'*';

        if ($text === '*') {
            abort(400);
        }

        $query = Text::search($text)
            ->with('searchable')
        ;

        $morphMap = [
            'trips' => Trip::class,
            'places' => Place::class,
            'users' => User::class,
            'hashtags' => Tag::class,
        ];

        if ($type) {
            if (!isset($morphMap[$type])) {
                abort(400);
            }

            $query->where('searchable_type', $morphMap[$type]);
        }

        $query->where('searchable_type', '!=', Post::class);

        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query
            ->take($limit+1)
            ->skip($offset)
        ;

        $items = $query->get();

        $cursor = new Cursor($offset);
        if ($items->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $cursor->setCount($query->count());

        $collection = new Collection($items->slice(0, $limit), new TextTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
