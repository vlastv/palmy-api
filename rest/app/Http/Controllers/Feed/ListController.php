<?php

namespace Palmy\Http\Controllers\Feed;

use Illuminate\Http\Request;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\StatusTransformer;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();

        $first = $request->get('first', 10);

        $items = $user->feed->take($first);

        $resource = new Collection($items ?: [], new StatusTransformer($request->user()), 'items');

        $resource->setMetaValue('kind', 'itemList');

        return response()->resource($resource);
    }
}
