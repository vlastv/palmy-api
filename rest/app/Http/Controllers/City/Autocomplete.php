<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 02.10.17
 * Time: 3:48
 */

namespace Palmy\Http\Controllers\City;


use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\City;
use Palmy\Country;
use Palmy\Transformer\CityTransformer;

class Autocomplete
{
    public function __invoke(Request $request, Country $country)
    {
        $limit = (int)$request->get('first') ?: 20;
        $offset = (int)$request->get('after') ?: 0;

        $builder = City::query()
            ->take($limit + 1)
            ->skip($offset);

        $builder->where('country_id', $country->getKey());

        if ($query = $request->get('query')) {
            $builder->where('name', 'like', $query.'%');
        }

        $cities = $builder->get();

        $cursor = new Cursor($offset);
        if ($cities->count() > $limit) {
            $cursor->setNext($offset + $limit);
        }

        $collection = new Collection($cities->slice(0, $limit), new CityTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
