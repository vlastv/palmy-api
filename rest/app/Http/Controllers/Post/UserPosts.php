<?php

namespace Palmy\Http\Controllers\Post;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Transformer\PostTransformer;
use Palmy\User;

class UserPosts
{
    public function __invoke(Request $request, User $user)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $user->posts()
            ->latest()
            ->take($limit+1)
            ->skip($offset)
        ;

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $cursor->setCount($query->count());

        $collection = new Collection($posts->slice(0, $limit), new PostTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
