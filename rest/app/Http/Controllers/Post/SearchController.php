<?php

namespace Palmy\Http\Controllers\Post;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Place;
use Palmy\Post;
use Palmy\Transformer\PostTransformer;
use Palmy\Trip;

class SearchController extends Controller
{
    public function showPostsByPlace(Request $request, Place $place)
    {
        $query = Post::where('place_id', $place->getKey());

        return $this->do($request, $query);
    }

    public function showPostsByTrip(Request $request, Trip $trip)
    {
        $query = Post::where('trip_id', $trip->getKey());

        return $this->do($request, $query);
    }

    private function do(Request $request, $query)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query
            ->latest()
            ->take($limit+1)
            ->skip($offset)
        ;

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $cursor->setCount($query->count());

        $collection = new Collection($posts->slice(0, $limit), new PostTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
