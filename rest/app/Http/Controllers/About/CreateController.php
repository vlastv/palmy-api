<?php

namespace Palmy\Http\Controllers\About;

use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Http\Controllers\Controller;
use Palmy\User;

class CreateController extends Controller
{
    public function __invoke(Request $request)
    {
        $data = $request->all();


        $this->validator($data)->validate();

        event(new Registered($user = $this->create($data)));

        return $user;
    }

    private function validator(array $data)
    {
        if (isset($data['email'])) {
            $canonicalizer = new EmailCanonicalizer();

            $data['email_canonical'] = $canonicalizer->canonicalize($data['email']);
        }

        return Validator::make($data, [
            'email_canonical' => 'required|string|email|max:255|unique:users,email_canonical',
            'password' => 'required|string|min:8',
        ]);
    }

    private function create(array $data)
    {
        return User::create([
            'email' => $data['email'],
            'password' => bcrypt($data['password'])
        ]);
    }
}
