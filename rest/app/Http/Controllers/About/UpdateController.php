<?php

namespace Palmy\Http\Controllers\About;

use Illuminate\Support\Arr;
use Illuminate\Validation\Factory as ValidationFactory;
use Palmy\Http\Controllers\Controller;
use Palmy\Http\Requests\UpdateProfile;
use Palmy\Transformer\UserTransformer;

class UpdateController extends Controller
{
    /**
     * @var \Illuminate\Validation\Validator
     */
    private $validator;

    public function __construct(ValidationFactory $validator)
    {
        $this->validator = $validator;
    }

    public function __invoke(UpdateProfile $request)
    {
        $user = $request->user();

        $map = [
            'firstName' => 'first_name',
            'lastName' => 'last_name',
            'username' => 'username',
            'email' => 'email',
            'gender' => 'gender',
            'birthdayDate' => 'birthday',
            'phone.number' => 'phone_number',
            'country' => 'country',
            'city' => 'city',
            'avatar' => 'avatar_id',
            'cover' => 'cover_id'
        ];

        $data = $request->intersect(array_keys($map));

        if (isset($data['password'])) {
            $user->password = bcrypt($data['password']);
        }

        foreach ($map as $key=>$attribute) {
            //            if (null !== $value = Arr::get($data, $key)) {
            $user[$attribute] = Arr::get($data, $key, $user[$attribute]);
            //            }
        }

        $user->save();

        return response()->item($user, new UserTransformer($user));
    }
}
