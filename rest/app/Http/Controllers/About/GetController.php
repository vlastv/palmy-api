<?php

namespace Palmy\Http\Controllers\About;

use Illuminate\Http\Request;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\UserTransformer;

class GetController extends Controller
{
    public function __invoke(Request $request)
    {
        return response()->item($request->user(), new UserTransformer($request->user()));
    }
}
