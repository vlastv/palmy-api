<?php

namespace Palmy\Http\Controllers;

use Cog\Likeable\Contracts\HasLikes as Likeable;
use Palmy\Post;
use Palmy\Transformer\PostTransformer;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;

class LikesController extends Controller
{
    public function likePost($postId)
    {
        $post = Post::find($postId);
        $this->like($post);

        return response()->item($post, new PostTransformer());
    }

    public function unlikePost($postId)
    {
        $post = Post::find($postId);
        $this->unlike($post);

        return response()->item($post, new PostTransformer());
    }

    public function likeTrip($tripId)
    {
        $trip = Trip::find($tripId);
        $this->like($trip);

        return response()->item($trip, new TripTransformer());
    }

    public function unlikeTrip($tripId)
    {
        $trip = Trip::find($tripId);
        $this->unlike($trip);

        return response()->item($trip, new TripTransformer());
    }

    private function like(Likeable $lakeable)
    {
        $lakeable->like();
    }

    private function unlike(Likeable $likeable)
    {
        $likeable->unlike();
    }
}
