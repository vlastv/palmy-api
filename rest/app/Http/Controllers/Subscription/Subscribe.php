<?php

namespace Palmy\Http\Controllers\Subscription;

use Illuminate\Http\Request;
use League\Fractal\TransformerAbstract;
use Palmy\Http\Controllers\Controller;
use Palmy\Place;
use Palmy\Transformer\PlaceTransformer;

class Subscribe extends Controller
{
    private function process(Request $request, $target, TransformerAbstract $transformer)
    {
        $user = $request->user();

        $user->subscribe($target);

        return response()->item($target, $transformer);

    }

    public function toPlace(Request $request, Place $place)
    {
        return $this->process($request, $place, new PlaceTransformer($request->user()));
    }
}