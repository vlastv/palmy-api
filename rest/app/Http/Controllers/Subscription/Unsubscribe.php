<?php

namespace Palmy\Http\Controllers\Subscription;

use Illuminate\Http\Request;
use League\Fractal\TransformerAbstract;
use Palmy\Place;
use Palmy\Tag;
use Palmy\Transformer\PlaceTransformer;
use Palmy\Transformer\TagTransformer;

class Unsubscribe
{
    private function process(Request $request, $target, TransformerAbstract $transformer)
    {
        $user = $request->user();

        $user->unsubscribe($target);

        return response()->item($target, $transformer);
    }

    public function fromPlace(Request $request, Place $place)
    {
        return $this->process($request, $place, new PlaceTransformer($request->user()));
    }
}
