<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use Palmy\Resource;
use Palmy\Transformer\TripTransformer;
use Palmy\Transformer\UserTransformer;
use Palmy\Trip;
use Palmy\User;

class LookupController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        $resource = Resource::lookup($id)->firstOrFail();

        switch ($resource->resource_type) {
            case User::class:
                $transformer = new UserTransformer($request->user());
                $target = $resource->resource()->withCount('trips', 'followers')->first();
                break;
            case Trip::class:
                $transformer = new TripTransformer($request->user());
                $target = $resource->resource;
                break;
            default:
                abort(404);
        }

        return response()->item($target, $transformer);
    }
}
