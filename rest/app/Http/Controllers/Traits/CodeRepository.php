<?php

namespace Palmy\Http\Controllers\Traits;

use Illuminate\Support\Str;
use Palmy\Auth\Emails\DatabaseTokenRepository;

trait CodeRepository
{
    protected function createCodeRepository()
    {
        $key = config('app.key');

        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        return new DatabaseTokenRepository(
            app('db')->connection(),
            app('hash'),
            'email_verifications',
            $key,
            120
        );
    }
}
