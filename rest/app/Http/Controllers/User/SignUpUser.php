<?php

namespace Palmy\Http\Controllers\User;

use Palmy\Http\Controllers\Controller;
use Palmy\Http\Controllers\Traits\CodeRepository;
use Palmy\Http\Requests\SignUpUser as Request;
use Palmy\Transformer\UserTransformer;
use Palmy\User;

class SignUpUser extends Controller
{
    use CodeRepository;

    public function __invoke(Request $request)
    {
        $codes = $this->createCodeRepository();

        $user = new User([
            'password' => bcrypt($request->get('password')),
            'username' => $request->get('username'),
            'first_name' => $request->get('firstName'),
            'last_name' => $request->get('lastName'),
            'email' => $request->get('email'),
        ]);

        abort_unless($codes->exists($user, $request->get('code')), 404);

        $user->save();

        $codes->delete($user);

        return response()->item($user, new UserTransformer($user));
    }
}
