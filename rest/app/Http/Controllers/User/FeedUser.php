<?php

namespace Palmy\Http\Controllers\User;

use Illuminate\Http\Request;
use League\Fractal\Manager;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\StatusTransformer;
use Palmy\User;

class FeedUser extends Controller
{
    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function __construct(Manager $fractal)
    {
        $this->fractal = $fractal;
    }

    public function __invoke(Request $request, User $user = null)
    {
        if (!$user->exists) {
            $user = $request->user();
        }

        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $user->feed()
            ->take($limit+1)
            ->skip($offset)
        ;

        if (!$user->is($request->user())) {
            $query->where('author_id', $user->id);
        }

        $posts = $query->get();

        $this->fractal->parseIncludes($request->get('include', ''));

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($posts->slice(0, $limit), new StatusTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
