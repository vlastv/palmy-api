<?php

namespace Palmy\Http\Controllers\User;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Place;
use Palmy\Tag;
use Palmy\Transformer\PlaceTransformer;
use Palmy\Transformer\TagTransformer;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;
use Palmy\User;

class Subscriptions
{
    public function __invoke(Request $request, User $user)
    {
        $corpus = $request->get('corpus');

        if (!$corpus) {
            abort(400);
        }

        if ($corpus === 'trip') {
            return $this->doTrip($request, $user);
        }

        if ($corpus === 'place') {
            return $this->doPlace($request, $user);
        }

        if ($corpus === 'hashtag') {
            return $this->doHashtag($request, $user);
        }

        return abort(400);
    }

    private function doTrip(Request $request, User $user)
    {
        return $this->do($user, Trip::class, $request, new TripTransformer($request->user()));
    }

    private function doPlace(Request $request, User $user)
    {
        return $this->do($user, Place::class, $request, new PlaceTransformer($request->user()));
    }

    private function doHashtag(Request $request, User $user)
    {
        return $this->do($user, Tag::class, $request, new TagTransformer($request->user()));
    }

    private function do($user, $class, $request, $transformer)
    {
        $query = $user->subscriptions($class);

        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $query
            ->take($limit+1)
            ->skip($offset)
        ;

        $subscribables = $query->get();

        $cursor = new Cursor($offset);
        if ($subscribables->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($subscribables->slice(0, $limit), $transformer);
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
