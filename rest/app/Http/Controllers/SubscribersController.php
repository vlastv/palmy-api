<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;

class SubscribersController extends Controller
{
    public function store(Request $request, $id)
    {
        $target = Trip::findOrFail($id);
        $user = $request->user();

        $user->subscribe($target);

        return response()->item($target, new  TripTransformer($user));
    }

    public function destroy(Request $request, $id)
    {
        $target = Trip::findOrFail($id);
        $user = $request->user();

        $user->unsubscribe($target);

        return response()->item($target, new  TripTransformer($user));
    }
}
