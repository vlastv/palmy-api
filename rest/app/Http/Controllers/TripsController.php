<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;
use Palmy\User;

class TripsController extends Controller
{
    public function destroy(Request $request, Trip $trip)
    {
        $this->authorize('delete', $trip);

        $trip->delete();

        return response('', 204);
    }

    public function subscriptions(Request $request, User $user)
    {
        $query = $user->subscriptions(Trip::class);

        $query->take($request->get('pageSize') ?: 20);

        $trips = $query->get();

        return response()->collection($trips, new TripTransformer($request->user()));
    }
}
