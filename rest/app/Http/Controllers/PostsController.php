<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use Palmy\Post;
use Palmy\Transformer\PostTransformer;

class PostsController extends Controller
{
    public function index(Request $request)
    {
        $query = Post::query();
        $query->with('place', 'participants', 'photos');

        if ($trip = $request->get('trip')) {
            $query->where('trip_id', $trip);
        }

        $posts = $query->take($request->get('pageSize') ?: 20)->get();

        return response()->collection($posts, new PostTransformer($request->user()));
    }

    public function destroy(Request $request, Post $post)
    {
        $this->authorize('delete', $post);

        $post->delete();

        return response('', 204);
    }
}
