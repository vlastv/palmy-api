<?php

namespace Palmy\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\UserTransformer;
use Palmy\User;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $query = $request->get('query');
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;
        $sort = $request->get('sort', $request->get('corpus'));

        $builder = User::query()
            ->take($limit+1)
            ->skip($offset)
        ;

        if ($query) {
            $builder->search($query);
        }

        switch ($sort) {
            default:
                $builder->withCount('medias', 'trips', 'followers')
                    ->orderBy(DB::raw('medias_count+trips_count+followers_count'), 'desc')
                ;
        }

        $users = $builder->get();

        $cursor = new Cursor($offset);
        if ($users->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($users->slice(0, $limit), new UserTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
