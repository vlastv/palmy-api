<?php

namespace Palmy\Http\Controllers\Users;

use Illuminate\Http\Request;
use Palmy\Canonicalizer\FacebookCanonicalizer;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\UserTransformer;
use Palmy\User;

class GetController extends Controller
{
    public function __construct()
    {
    }

    public function __invoke(Request $request, $username)
    {
        if (is_numeric($username)) {
            $query = User::whereKey($username);
        } else {
            $canonicalizer = new FacebookCanonicalizer();

            $canonicalUsername = $canonicalizer->canonicalize($username);

            $query = User::where('username_canonical', $canonicalUsername);
        }

        $query->withCount(['trips', 'followers']);

        $user = $query->firstOrFail();

        return response()->item($user, new UserTransformer($request->user()));
    }
}
