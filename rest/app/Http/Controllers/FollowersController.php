<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use Palmy\Transformer\UserTransformer;
use Palmy\Trip;
use Palmy\User;

class FollowersController
{
    public function index(Request $request, $userId)
    {
        $target = User::findOrFail($userId);

        $paginator = $target->followers()->paginate(
            $request->get('pageSize', 20)
        );

        $followers = $paginator->getCollection();

        $collection = new Collection($followers, new UserTransformer($request->user()));
        $collection->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return response()->resource($collection);
    }

    public function store(Request $request, User $user)
    {
        $auth = $request->user();

        $auth->follow($user);
        $auth->subscribe($user->trips->all(), Trip::class);

        return response()->item($user, new UserTransformer($auth));
    }

    public function destroy(Request $request, User $user)
    {
        $auth = $request->user();

        $auth->unfollow($user, User::class);
        $auth->unsubscribe($user->trips->all(), Trip::class);

        return response()->item($user, new UserTransformer($auth));
    }
}
