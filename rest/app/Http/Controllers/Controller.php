<?php

namespace Palmy\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function collection(Request $request, $query, $transformer)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $query
            ->latest()
            ->take($limit+1)
            ->skip($offset)
        ;

        $items = $query->get();

        $cursor = new Cursor($offset);
        if ($items->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $cursor->setCount($query->count());

        $collection = new Collection($items->slice(0, $limit), $transformer);
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
