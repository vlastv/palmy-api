<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use Palmy\Comment;
use Palmy\Contracts\Commentable;
use Palmy\Http\Requests\AddComment;
use Palmy\Post;
use Palmy\Transformer\CommentTransformer;
use Palmy\Trip;

class CommentsController extends Controller
{
    public function storeForTrip(AddComment $request, Trip $trip)
    {
        return $this->doStore($request, $trip);
    }

    public function storeForPost(AddComment $request, Post $post)
    {
        return $this->doStore($request, $post);
    }

    public function destroy(Comment $comment)
    {
        $comment->delete();

        return response('', 204);
    }

    public function indexForPost(Request $request, Post $post)
    {
        return $this->doIndex($request, $post);
    }

    public function indexForTrip(Request $request, Trip $trip)
    {
        return $this->doIndex($request, $trip);
    }

    private function doIndex(Request $request, Commentable $commentable)
    {
        $paginator = $commentable->comments()->paginate(
            $request->get('pageSize', 20)
        );

        $comments = $paginator->getCollection();

        $collection = new Collection($comments, new CommentTransformer($request->user()));
        $collection->setPaginator(new IlluminatePaginatorAdapter($paginator));

        return response()->resource($collection);
    }

    private function doStore(AddComment $request, Commentable $commentable)
    {
        $comment = new Comment([
            'message' => $request->get('message'),
        ]);

        $comment->withDefaultOwner($request->user());

        $commentable->comments()->save($comment);

        return response()->item($comment, new CommentTransformer($request->user()));
    }
}
