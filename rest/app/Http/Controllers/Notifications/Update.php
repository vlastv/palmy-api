<?php

namespace Palmy\Http\Controllers\Notifications;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Notifications\DatabaseNotification;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\NotificationTransformer;

class Update extends Controller
{
    public function __invoke(Request $request, DatabaseNotification $notification)
    {
        $auth = $request->user();

        if ($notification->notifiable->isNot($auth)) {
            throw new AuthorizationException();
        }

        $notification->markAsRead();

        return response()->item($notification, new NotificationTransformer($auth));
    }
}
