<?php

namespace Palmy\Http\Controllers\Notifications;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Notifications\FriendRegistered;
use Palmy\Notifications\NewComment;
use Palmy\Transformer\NotificationTransformer;
use Palmy\User;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $auth = $request->user();

        return $this->collection(
            $request,
            $auth->notifications()
                ->latest(),
            new NotificationTransformer($auth)
        );
    }
}
