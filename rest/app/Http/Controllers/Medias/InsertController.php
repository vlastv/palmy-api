<?php

namespace Palmy\Http\Controllers\Medias;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Palmy\Http\Controllers\Controller;
use Palmy\Media;
use Palmy\Transformer\MediaTransformer;
use Symfony\Component\HttpFoundation\File\MimeType\ExtensionGuesser;

class InsertController extends Controller
{
    public function __invoke(Request $request)
    {
        $user = $request->user();

        $data = $request->all();

        $guesser = ExtensionGuesser::getInstance();

        $extension = $guesser->guess($data['mimeType']);

        list($namespace, $type) = explode('_', strtolower($data['context']), 2);

        $random = Str::random(40);

        $key = sprintf(
            '%s/%s/%s/%s.%s',
            $namespace,
            Str::plural($type),
            substr($random, 0, 8),
            substr($random, 8),
            $extension
        );

        $s3 = Storage::cloud()->getDriver()->getAdapter();
        $client = Storage::cloud()->getDriver()->getAdapter()->getClient();

        $cmd = $client->getCommand('PutObject', [
            'Bucket' => $s3->getBucket(),
            'ACL' => 'public-read',
            'Key' => $s3->applyPathPrefix($key),
        ]);

        $request = $client->createPresignedRequest($cmd, '+20 minutes');

        $media = Media::create([
            'name' => $data['name'],
            'mime_type' => $data['mimeType'],
            'user_id' => $user->getKey(),
            'provider_name' => 's3',
            'provider_reference' => $key,
            'context' => $data['context']
        ]);

        return response()->item($media, new MediaTransformer($user, (string) $request->getUri()));
    }
}
