<?php

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Transformer\TagTransformer;
use Palmy\User;

class UsedHashtags
{
    public function __invoke(Request $request, User $user)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $user->usages()
            ->take($limit+1)
            ->skip($offset)
        ;

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($posts->slice(0, $limit), new TagTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
