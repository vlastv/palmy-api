<?php

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use Palmy\Tag;
use Palmy\Transformer\TagTransformer;

class Hashtag
{
    public function __invoke(Request $request, $slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();

        return response()->item($tag, new TagTransformer($request->user()));
    }
}
