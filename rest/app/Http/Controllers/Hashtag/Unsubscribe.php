<?php

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use Palmy\Tag;
use Palmy\Transformer\TagTransformer;

class Unsubscribe
{
    public function __invoke(Request $request, Tag $tag)
    {
        $user = $request->user();

        $user->unsubscribe($tag);

        return response()->item($tag, new TagTransformer($request->user()));
    }
}
