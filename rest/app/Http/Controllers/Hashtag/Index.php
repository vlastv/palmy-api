<?php

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Http\Controllers\Controller;
use Palmy\Tag;
use Palmy\Transformer\TagTransformer;

class Index extends Controller
{
    public function __invoke(Request $request)
    {

        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = Tag::query()
//            ->with('usages', function($query) {
//                $query->orderBy('value', 'desc')
//            })
            ->take($limit+1)
            ->skip($offset)
        ;

        if ($sort = $request->get('sort')) {
            switch ($sort) {
                case 'POPULARITY':
//                    $query->orderBy('usages.value', 'desc');
                    break;
            }
        }

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($posts->slice(0, $limit), new TagTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
