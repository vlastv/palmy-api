<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 21.08.17
 * Time: 12:54
 */

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Tag;
use Palmy\Transformer\PostTransformer;

class PostsByHashtag
{
    public function __invoke(Request $request, $slug)
    {
        $tag = Tag::where('slug', $slug)->firstOrFail();

        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = $tag->posts()
            ->latest()
            ->take($limit+1)
            ->skip($offset)
        ;

        $posts = $query->get();

        $cursor = new Cursor($offset);
        if ($posts->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $collection = new Collection($posts->slice(0, $limit), new PostTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
