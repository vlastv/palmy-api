<?php

namespace Palmy\Http\Controllers\Hashtag;

use Illuminate\Http\Request;
use Palmy\Tag;
use Palmy\Transformer\TagTransformer;

class Subscribe
{
    public function __invoke(Request $request, Tag $tag)
    {
        $user = $request->user();

        $user->subscribe($tag);

        return response()->item($tag, new TagTransformer($request->user()));
    }
}
