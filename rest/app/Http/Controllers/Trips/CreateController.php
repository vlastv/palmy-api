<?php

namespace Palmy\Http\Controllers\Trips;

use Illuminate\Http\Request;
use League\Fractal\Manager;
use Palmy\Http\Controllers\Controller;
use Palmy\Item;
use Palmy\Media;
use Palmy\Repositories\PlaceRepository;
use Palmy\Transformer\StatusTransformer;
use Palmy\Trip;

class CreateController extends Controller
{
    /**
     * @var \Palmy\Repositories\PlaceRepository
     */
    private $places;
    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function __construct(PlaceRepository $places, Manager $fractal)
    {
        $this->places = $places;
        $this->fractal = $fractal;
    }

    public function __invoke(Request $request)
    {
        $user = $request->user();
        $data = $request->all();
        $this->validateWith($this->rules(), $request);

        $trip = (new Trip)->fill([
            'name' => $data['name'],
            'start' => \DateTime::createFromFormat('Y-m-d', $data['startDate']),
            'end' => isset($data['endDate']) ? \DateTime::createFromFormat('Y-m-d', $data['endDate']) : null,
            'private' => $data['private'] !== false,
        ]);
        $trip->user()->associate($user);

        if (isset($data['cover'])) {
            $trip->cover()->associate(Media::find($data['cover']));
        }

        if (isset($data['place'])) {
            $trip->place()->associate($this->places->find($data['place']));
        }

        $trip->save();

        if (isset($data['participants'])) {
            $trip->participants()->delete();
            foreach ((array) $data['participants'] as $participant) {
                $trip->participants()->attach($participant);
            }
        }

        foreach ($user->followers as $follower) {
            if ($follower->is($user)) {
                continue;
            }

            $follower->subscribe($trip);

            $status = new Item(['fact' => 'trip_create']);
            $status->item()->associate($trip);
            $status->author_id = $user->getKey();
            $status->user_id = $follower->getKey();

            $follower->feed()->save($status);
        }

        $this->fractal->parseIncludes('post');
        $status = new Item(['fact' => 'trip_create']);
        $status->item()->associate($trip);
        $status->author_id = $user->getKey();

        $user->feed()->save($status);

        return response()->item($status, new StatusTransformer($user), 201);
    }

    protected function rules()
    {
        return [
            'name' => 'required|min:3',
            'startDate' => [
                'required',
                'date_format:Y-m-d'
            ],
            'endDate' => [
                'sometimes',
                'date_format:Y-m-d',
                'after:startDate'
            ],
            'private' => [
                'required',
                'boolean'
            ],
            'cover' => [
                'nullable',
                'exists:media,id'
            ]
        ];
    }
}
