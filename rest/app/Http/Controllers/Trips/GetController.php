<?php

namespace Palmy\Http\Controllers\Trips;

use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;

class GetController extends Controller
{
    public function __invoke($tripId)
    {
        $trip = Trip::find($tripId);

        return response()->item($trip, new TripTransformer());
    }
}
