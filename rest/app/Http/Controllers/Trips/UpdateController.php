<?php

namespace Palmy\Http\Controllers\Trips;

use Palmy\Http\Controllers\Controller;
use Palmy\Http\Requests\UpdateTrip;
use Palmy\Media;
use Palmy\Repositories\PlaceRepository;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;

class UpdateController extends Controller
{
    /**
     * @var \Palmy\Repositories\PlaceRepository
     */
    private $places;

    public function __construct(PlaceRepository $places)
    {
        $this->places = $places;
    }

    public function __invoke(UpdateTrip $request, $id)
    {
        $data = $request->all();

        $trip = Trip::findOrFail($id);

        $this->authorize('update', $trip);

        if (array_key_exists('name', $data)) {
            $trip->name = $data['name'];
        }

        if (array_key_exists('startDate', $data)) {
            $trip->start = \DateTime::createFromFormat('Y-m-d', $data['startDate']);
        }

        if (array_key_exists('endDate', $data)) {
            $trip->end = $data['endDate'] ? \DateTime::createFromFormat('Y-m-d', $data['endDate']) : null;
        }

        if (array_key_exists('private', $data)) {
            $trip->private = $data['private'] !== false;
        }

        if (array_key_exists('cover', $data)) {
            if ($data['cover']) {
                $trip->cover()->associate(Media::find($data['cover']));
            } else {
                $trip->cover()->dissociate();
            }
        }

        if (array_key_exists('place', $data)) {
            if ($data['place']) {
                $trip->place()->associate($this->places->find($data['place']));
            } else {
                $trip->place()->dissociate();
            }
        }

        if (array_key_exists('participants', $data)) {
            $trip->participants()->sync((array) $data['participants']);
        }

        $trip->save();

        return response()->item($trip, new TripTransformer());
    }
}
