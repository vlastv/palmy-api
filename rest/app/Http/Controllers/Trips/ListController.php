<?php

namespace Palmy\Http\Controllers\Trips;

use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Transformer\TripTransformer;
use Palmy\Trip;

class ListController
{
    public function __invoke(Request $request, $userId = null)
    {
        $limit = (int) $request->get('first') ?: 20;
        $offset  = (int) $request->get('after') ?: 0;

        $query = Trip::query()
            ->with('user', 'participants', 'cover', 'resource', 'place', 'likesCounter')
            ->take($limit+1)
            ->skip($offset)
        ;

        if (!empty($orders = $request->query('orderBy'))) {
            $orders = explode(',', $orders);
            foreach ($orders as $order) {
                $query = $query->$order();
            }
        }

        if ($userId) {
            $query->where('user_id', $userId);
        }

        $trips = $query->get();

        $cursor = new Cursor($offset);
        if ($trips->count() > $limit) {
            $cursor->setNext($offset+$limit);
        }

        $cursor->setCount($query->count('id'));

        $collection = new Collection($trips->slice(0, $limit), new TripTransformer($request->user()));
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
