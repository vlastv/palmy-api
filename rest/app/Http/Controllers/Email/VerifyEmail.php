<?php

namespace Palmy\Http\Controllers\Email;

use Palmy\Http\Controllers\Controller;
use Palmy\Http\Controllers\Traits\CodeRepository;
use Palmy\Http\Requests\VerifyEmail as Request;
use Palmy\VerificationEmail;

class VerifyEmail extends Controller
{
    use CodeRepository;

    public function __invoke(Request $request)
    {
        $codes = $this->createCodeRepository();

        $verification = new VerificationEmail($request->get('email'));

        if (!$codes->exists($verification, $request->get('code'))) {
            abort(404);
        }

        return response()->json(new \stdClass(), 201);
    }
}
