<?php

namespace Palmy\Http\Controllers\Email;

use Palmy\Http\Controllers\Controller;
use Palmy\Http\Controllers\Traits\CodeRepository;
use Palmy\Http\Requests\ObtainEmail as Request;
use Palmy\VerificationEmail;

class ObtainEmail extends Controller
{
    use CodeRepository;

    public function __invoke(Request $request)
    {
        $verification = new VerificationEmail($request->get('email'));

        $codes = $this->createCodeRepository();

        $verification->sendEmailVerifyNotification(
            $codes->create($verification)
        );

        return response()->json(new \stdClass, 201);
    }
}
