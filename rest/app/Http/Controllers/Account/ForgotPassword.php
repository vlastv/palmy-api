<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 01.09.17
 * Time: 12:28
 */

namespace Palmy\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Palmy\Http\Controllers\Controller;

class ForgotPassword extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validateEmail($request);
        
        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response === Password::RESET_LINK_SENT
            ? response('', 204)
            : abort(400)
            ;
    }

    protected function validateEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email']);
    }

    public function broker()
    {
        return Password::broker();
    }
}
