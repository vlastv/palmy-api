<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 01.09.17
 * Time: 15:57
 */

namespace Palmy\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Palmy\Http\Controllers\Controller;
use Palmy\User;

class ChangePassword extends Controller
{
    public function __invoke(Request $request)
    {
        $this->validate($request, $this->rules(), $this->messages());

        $credentials = $request->only('password', 'current');

        $user = $request->user();

        $this->resetPassword($user, $credentials['password']);

        return [];
    }

    private function rules(): array
    {
        return [
            'current' => 'required|user_password',
            'password' => 'required|min:6'
        ];
    }

    private function messages(): array
    {
        return [
            'min' => 'length.min::min',
            'user_password' => 'userPassword'
        ];
    }

    protected function resetPassword(User $user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
    }
}
