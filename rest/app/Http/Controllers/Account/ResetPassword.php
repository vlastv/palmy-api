<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 01.09.17
 * Time: 12:31
 */

namespace Palmy\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Palmy\Http\Controllers\Controller;
use Palmy\User;

class ResetPassword extends Controller
{
    public function __invoke(Request $request)
    {
        $request->query->set('password_confirmation', $request->input('password'));

        $this->validate($request, $this->rules());

        $response = $this->broker()->reset(
            $this->credentials($request),
            function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        return $response === Password::PASSWORD_RESET
            ? response('', 204)
            : abort(400);
    }

    protected function rules()
    {
        return [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
    }

    public function broker()
    {
        return Password::broker();
    }

    protected function credentials(Request $request)
    {
        return $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );
    }

    protected function resetPassword(User $user, $password)
    {
        $user->forceFill([
            'password' => bcrypt($password),
            'remember_token' => Str::random(60),
        ])->save();
    }
}
