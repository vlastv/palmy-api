<?php

namespace Palmy\Http\Controllers\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Palmy\Auth\Emails\DatabaseTokenRepository;
use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Http\Controllers\Controller;
use Palmy\Transformer\UserTransformer;
use Palmy\User;

class RegisterController extends Controller
{
    public function __invoke(Request $request)
    {
        $email = $request->get('email');

        $canonicalizer = new EmailCanonicalizer();

        $canonicalEmail = $canonicalizer->canonicalize($email);

        $request->merge([
            'email' => $canonicalEmail
        ]);

        $this->validateEmail($request);

        $user = User::create([
            'email' => $email
        ]);

        $codes = $this->createCodeRepository();

        /* @var $user \Palmy\Contracts\Auth\NeedVerificationEmail */
        $user->sendEmailVerifyNotification(
            $codes->create($user)
        );

        return response()->item($user, new UserTransformer($request->user()));
    }

    private function validateEmail(Request $request)
    {
        $this->validate($request, [
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email_canonical'),
            ],
        ]);
    }

    private function createCodeRepository()
    {
        $key = config('app.key');

        if (Str::startsWith($key, 'base64:')) {
            $key = base64_decode(substr($key, 7));
        }

        return new DatabaseTokenRepository(
            app('db')->connection(),
            app('hash'),
            'email_verifications',
            $key,
            120
        );
    }
}
