<?php

namespace Palmy\Http\Controllers;

use Illuminate\Http\Request;
use Palmy\Transformer\UserTransformer;
use Palmy\User;

class UsersController extends Controller
{
    public function followings(Request $request, $userId)
    {
        $user = User::findOrFail($userId);

        $followings = $user->followings()
            ->take($request->get('pageSize') ?: 20)
            ->get();

        return response()->collection($followings, new UserTransformer($request->user()));
    }
}
