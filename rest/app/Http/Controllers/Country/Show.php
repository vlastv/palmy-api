<?php
/**
 * Created by PhpStorm.
 * User: vlastv
 * Date: 02.10.17
 * Time: 3:58
 */

namespace Palmy\Http\Controllers\Country;


use Palmy\Country;
use Palmy\Transformer\CountryTransformer;

class Show
{
    public function __invoke(Country $country)
    {
        return response()->item($country, new CountryTransformer());
    }
}
