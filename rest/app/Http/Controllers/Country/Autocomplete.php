<?php

namespace Palmy\Http\Controllers\Country;


use Illuminate\Http\Request;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Palmy\Country;
use Palmy\Transformer\CountryTransformer;
use Palmy\Transformer\PostTransformer;

class Autocomplete
{
    public function __invoke(Request $request)
    {
        $limit = (int)$request->get('first') ?: 20;
        $offset = (int)$request->get('after') ?: 0;

        $builder = Country::query()
            ->take($limit + 1)
            ->skip($offset);

        if ($query = $request->get('query')) {
            $builder->where('name', 'like', $query.'%');
        }

        $countries = $builder->get();

        $cursor = new Cursor($offset);
        if ($countries->count() > $limit) {
            $cursor->setNext($offset + $limit);
        }



        $collection = new Collection($countries->slice(0, $limit), new CountryTransformer());
        $collection->setCursor($cursor);

        return response()->resource($collection);
    }
}
