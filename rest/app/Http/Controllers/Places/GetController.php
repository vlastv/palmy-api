<?php

namespace Palmy\Http\Controllers\Places;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Flysystem\AdapterInterface;
use Palmy\Http\Controllers\Controller;
use Palmy\Media;
use Palmy\Place;
use Palmy\Transformer\PlaceTransformer;

class GetController extends Controller
{
    public function __invoke(Request $request, $id)
    {
        return response()->item($this->find($id), new PlaceTransformer($request->user()));
    }

    private function find($id)
    {
        list($scope, $reference) = explode('|', $id, 2);

        $place = Place::where([
            'scope' => $scope,
            'reference' => $reference
        ])->first();

        if ($place !== null) {
            return $place;
        }

        $result = $this->proxy($reference);

        if ($result['status'] !== 'OK') {
            return abort(404);
        }

        $place = new Place([
            'scope' => $scope,
            'reference' => $reference,
            'name' => $result['result']['name'],
            'latitude' => $result['result']['geometry']['location']['lat'],
            'longitude' => $result['result']['geometry']['location']['lng'],
        ]);

        if (null !== $media = $this->downloadPhoto($result)) {
            $place->cover()->associate($media);
        }

        $place->save();

        return $place;
    }

    private function proxy($id)
    {
        $client = new Client();

        $url = 'https://maps.googleapis.com/maps/api/place/details/json'
            . '?' . http_build_query([
                'key' => config('services.google.key'),
                'placeid' => $id,
                'language' => 'ru'
            ]);

        $response = $client->get($url);

        return \GuzzleHttp\json_decode($response->getBody(), true);
    }

    private function downloadPhoto($response)
    {
        if (null === $reference = $this->photoReference($response)) {
            return;
        }

        $url = $this->photoLink($reference);

        $client = new Client();
        $response = $client->get($url, ['stream' => true]);

        $random = Str::random(40);

        $key = sprintf(
            '%s/%s/%s/%s.%s',
            'place',
            Str::plural('cover'),
            substr($random, 0, 8),
            substr($random, 8),
            'jpg'
        );

        $fs = Storage::cloud();
        if (!$fs->put(
                $key,
                $response->getBody()->getContents(),
                [
                    'ACL' => 'public-read',
                ]
        )) {
            return;
        }

        $media = Media::create([
            'name' => 'google_'.md5($reference),
            'mime_type' => 'image/jpeg',
            'provider_name' => 's3',
            'provider_reference' => $key,
            'context' => 'PLACE',
        ]);

        return $media;
    }

    private function photoReference($response)
    {
        if (!array_key_exists('photos', $response['result'])) {
            return null;
        }

        return $response['result']['photos'][0]['photo_reference'];
    }

    private function photoLink($reference)
    {
        return sprintf('https://maps.googleapis.com/maps/api/place/photo?maxwidth=%d&photoreference=%s&key=%s',
            1600,
            $reference,
            config('services.google.key'));
    }
}
