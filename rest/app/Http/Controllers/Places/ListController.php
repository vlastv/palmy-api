<?php

namespace Palmy\Http\Controllers\Places;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Palmy\Http\Controllers\Controller;

class ListController extends Controller
{
    public function __invoke(Request $request)
    {
        $text = $request->get('query', '');

        if (trim($text) === '') {
            abort(422);
        }

        return response()->json([
            'kind' => 'placeList',
            'data' => $this->find($text)
        ]);
    }

    private function find($text): array
    {
        $client = new Client();

        $url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json'
            . '?' . http_build_query([
                'key' => config('services.google.key'),
//                'type' => 'geocode',
                'input' => $text,
                'language' => 'ru'
            ]);

        $response = $client->get($url);

        $resource = \GuzzleHttp\json_decode($response->getBody(), true);

        if ($resource['status'] !== 'OK') {
            return [];
        }

        return array_map(function ($prediction) {
            return [
                'kind' => 'place',
                'id' => 'google'.'|'.$prediction['place_id'],
                'name' => $prediction['structured_formatting']['main_text'],
                'location' => [
                    'description' => $prediction['structured_formatting']['secondary_text'] ?? null,
                ]
            ];
        }, $resource['predictions']);
    }
}
