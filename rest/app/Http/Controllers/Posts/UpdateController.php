<?php

namespace Palmy\Http\Controllers\Posts;

use Palmy\Http\Controllers\Controller;
use Palmy\Http\Requests\PublishPost;
use Palmy\Post;
use Palmy\Repositories\PlaceRepository;
use Palmy\Transformer\PostTransformer;
use Palmy\Trip;

class UpdateController extends Controller
{
    /**
     * @var \Palmy\Repositories\PlaceRepository
     */
    private $places;

    public function __construct(PlaceRepository $places)
    {
        $this->places = $places;
    }

    public function __invoke(PublishPost $request, $id)
    {
        $post = Post::findOrFail($id);

        $this->authorize('update', $post);

        $data = $request->all();

        if (array_key_exists('message', $data)) {
            $post->message = $data['message'];
        }

        if (array_key_exists('private', $data)) {
            $post->private  =  $data['private'] !== false;
        }

        if (array_key_exists('place', $data)) {
            if ($data['place']) {
                $post->place()->associate($this->places->find($data['place']));
            } else {
                $post->place()->dissociate();
            }
        }

        if (array_key_exists('trip', $data)) {
            if ($data['trip']) {
                $post->trip()->associate($data['trip']);
            } else {
                $post->trip()->dissociate();
            }
        }

        if (array_key_exists('participants', $data)) {
            $post->participants()->sync($data['participants']);
        }

        if (array_key_exists('photos', $data)) {
            $post->photos()->sync($data['photos']);
        }

        $post->save();

        return response()->item($post, new PostTransformer());
    }
}
