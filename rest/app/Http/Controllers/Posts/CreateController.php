<?php

namespace Palmy\Http\Controllers\Posts;

use League\Fractal\Manager;
use Palmy\Http\Controllers\Controller;
use Palmy\Http\Requests\PublishPost;
use Palmy\Item;
use Palmy\Post;
use Palmy\Repositories\PlaceRepository;
use Palmy\Transformer\StatusTransformer;

class CreateController extends Controller
{
    /**
     * @var \Palmy\Repositories\PlaceRepository
     */
    private $places;
    /**
     * @var \League\Fractal\Manager
     */
    private $fractal;

    public function __construct(PlaceRepository $places, Manager $fractal)
    {
        $this->places = $places;
        $this->fractal = $fractal;
    }

    public function __invoke(PublishPost $request)
    {
        $user = $request->user();

        /** @var Post $post */
        $post = (new Post)->fill([
            'message' => $request->input('message'),
            'private' => $request->input('private') !== false,
        ]);

        $post->user()->associate($user);

        if (null !== $placeId = $request->input('place')) {
            $post->place()->associate($this->places->find($placeId));
        }

        if (null !== $tripId = $request->input('trip')) {
            $post->trip()->associate($tripId);
        }

        $post->save();

        if (null !== $participants = $request->input('participants')) {
            foreach ((array) $participants as $participant) {
                $post->participants()->attach($participant);
            }
        }

        if (null !== $photos = (array) $request->input('photos')) {
            foreach ($photos as $photo) {
                $post->photos()->attach($photo);
            }
        }

        foreach ($user->followers as $follower) {
            if ($follower->is($user)) {
                continue;
            }

            if ($post->trip && !$post->trip->isSubscribedBy($follower)) {
                continue;
            }

            $status = new Item(['fact' => 'post_publish']);
            $status->item()->associate($post);
            $status->author_id = $user->getKey();
            $status->user_id = $follower->getKey();

            $follower->feed()->save($status);
        }

        $this->fractal->parseIncludes('post');
        $status = new Item(['fact' => 'post_publish']);
        $status->item()->associate($post);
        $status->author_id = $user->getKey();

        $user->feed()->save($status);

        return response()->item($status, new StatusTransformer(), 201);
    }
}
