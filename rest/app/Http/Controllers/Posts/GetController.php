<?php

namespace Palmy\Http\Controllers\Posts;

use Palmy\Http\Controllers\Controller;
use Palmy\Post;
use Palmy\Transformer\PostTransformer;

class GetController extends Controller
{
    public function __invoke($postId)
    {
        $post = Post::find($postId);

        return response()->item($post, new PostTransformer());
    }
}
