<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Canonicalizer\FacebookCanonicalizer;

class UpdateProfile extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user();

        return [
            'birthdayDate' => [
                'sometimes',
                'nullable',
                'date'
            ],
            'username_canonical' => [
                'sometimes',
                Rule::unique('resources', 'username')->ignore($user->resource ? $user->resource->getKey() : null),
            ],
            'email_canonical' => [
                'sometimes',
                'required',
                'email',
                Rule::unique('users', 'email_canonical')->ignore($user['id']),
            ],
            'password' => [
                'sometimes',
                'required',
                'string',
                'min:8'
            ],
            'gender' => [
                'sometimes',
                'required',
                Rule::in(['male', 'female']),
            ]
        ];
    }

    protected function validationData()
    {
        $data = $this->all();

        if (isset($data['username'])) {
            $canonicalizer = new FacebookCanonicalizer();
            $data['username_canonical'] = $canonicalizer->canonicalize($data['username']);
        }

        if (isset($data['email'])) {
            $canonicalizer = new EmailCanonicalizer();
            $data['email_canonical'] = $canonicalizer->canonicalize($data['email']);
        }

        return $data;
    }
}
