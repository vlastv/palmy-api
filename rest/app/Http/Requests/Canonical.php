<?php

namespace Palmy\Http\Requests;

use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Canonicalizer\FacebookCanonicalizer;

trait Canonical
{
    protected function validationData(): array
    {
        $data = parent::validationData();

        if ($email = $this->get('email')) {
            $canonicalizer = new EmailCanonicalizer();

            $email = $canonicalizer->canonicalize($email);

            $data = array_merge($data, ['email' => $email]);
        }

        if ($username = $this->get('username')) {
            $canonicalizer = new FacebookCanonicalizer();

            $username = $canonicalizer->canonicalize($username);

            $data = array_merge($data, ['username' => $username]);
        }

        return $data;
    }
}
