<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PublishPost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $create = $this->getMethod() !== 'PATCH';

        return [
            'message' => [
                'nullable',
                'string',
            ],
            'trip' => [
                'nullable',
                'exists:trips,id'
            ],
            'private' => [
                $create ? null : 'sometimes',
                'required',
                'boolean',
            ],
            'photos' => [
                'array',
                'exists:media,id'
            ]
        ];
    }
}
