<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SignUpUser extends FormRequest
{
    use Canonical;

    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'firstName' => 'required',
            'lastName' => 'required',
            'email' => 'required|email|max:255|unique:users,email_canonical',
            'password' => 'required|string|min:6',
            'username' => [
                'sometimes',
                Rule::unique('resources', 'username'),
            ]
        ];
    }
}
