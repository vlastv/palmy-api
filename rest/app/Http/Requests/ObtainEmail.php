<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Palmy\Canonicalizer\EmailCanonicalizer;

class ObtainEmail extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
                Rule::unique('users', 'email_canonical'),
            ],
        ];
    }

    protected function validationData(): array
    {
        $data = $this->all();

        if ($this->has('email')) {
            $canonicalizer = new EmailCanonicalizer();

            $email = $canonicalizer->canonicalize($this->get('email'));

            $data = array_merge($data, ['email' => $email]);
        }

        return $data;
    }
}
