<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTrip extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'nullable|min:3',
            'startDate' => [
                'sometimes',
                'date_format:Y-m-d'
            ],
            'endDate' => [
                'nullable',
                'date_format:Y-m-d',
                'after:startDate'
            ],
            'private' => [
                'nullable',
                'boolean'
            ],
            'cover' => [
                'nullable',
                'exists:media,id'
            ]
        ];
    }
}
