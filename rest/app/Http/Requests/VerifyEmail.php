<?php

namespace Palmy\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Palmy\Canonicalizer\EmailCanonicalizer;

class VerifyEmail extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => [
                'required',
                'email',
            ],
            'code' => [
                'required',
            ]
        ];
    }

    protected function validationData(): array
    {
        $data = $this->all();

        if ($email = $this->get('email')) {
            $canonicalizer = new EmailCanonicalizer();

            $email = $canonicalizer->canonicalize($email);

            $data = array_merge($data, ['email' => $email]);
        }

        return $data;
    }
}
