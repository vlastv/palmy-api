<?php

Route::domain(config('app.url'))->group(function () {
    Route::get('/?externalCommand=resetPassword&externalCommandParams%5Bemail%5D={email}&externalCommandParams%5Btoken%5D={token}')->name('password.reset');
});
