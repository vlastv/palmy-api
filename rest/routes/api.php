<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'About'], function () {
    Route::post('/users', 'CreateController');
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::group(['namespace' => 'About'], function () {
        Route::get('/about', 'GetController');
        Route::patch('/about', 'UpdateController');
    });

    Route::group(['namespace' => 'Trips'], function () {
        Route::post('/trips', 'CreateController');
        Route::get('/trips/{tripId}', 'GetController');
        Route::patch('/trips/{postId}', 'UpdateController');
    });

    Route::delete('/trips/{trip}', 'TripsController@destroy');
    Route::get('/users/{user}/subscriptions', 'TripsController@subscriptions');

    Route::group(['namespace' => 'Posts'], function () {
        Route::post('/posts', 'CreateController');
        Route::get('/posts/{postId}', 'GetController');
        Route::patch('/posts/{postId}', 'UpdateController');
    });

    Route::get('/posts', 'PostsController@index');
    Route::delete('/posts/{post}', 'PostsController@destroy');

    Route::group(['namespace' => 'Notifications'], function () {
        Route::get('/notifications', 'ListController');
    });

    Route::group(['namespace' => 'Medias'], function () {
        Route::post('/medias', 'InsertController');
    });

    Route::group(['namespace' => 'Feed'], function () {
        Route::get('/feed', 'ListController');
    });

    Route::post('/posts/{id}/likes', 'LikesController@likePost');
    Route::delete('/posts/{id}/likes', 'LikesController@unlikePost');
    Route::post('/trips/{id}/likes', 'LikesController@likeTrip');
    Route::delete('/trips/{id}/likes', 'LikesController@unlikeTrip');

    Route::post('/trips/{trip}/comments', 'CommentsController@storeForTrip');
    Route::post('/posts/{post}/comments', 'CommentsController@storeForPost');
    Route::delete('/comments/{comment}', 'CommentsController@destroy');
    Route::get('/trips/{trip}/comments', 'CommentsController@indexForTrip');
    Route::get('/posts/{post}/comments', 'CommentsController@indexForPost');


    Route::post('/users/{user}/followers', 'FollowersController@store');
    Route::delete('/users/{user}/followers', 'FollowersController@destroy');
    Route::get('/users/{userId}/followers', 'FollowersController@index');
    Route::get('/users/{userId}/followings', 'UsersController@followings');

    Route::post('/trips/{tripId}/subscribers', 'SubscribersController@store');
    Route::delete('/trips/{tripId}/subscribers', 'SubscribersController@destroy');

    Route::get('/users/me/feed', 'User\FeedUser');
    Route::get('/users/{user}/feed', 'User\FeedUser');

    Route::get('/hashtags/{slug}', 'Hashtag\Hashtag');
    Route::get('/hashtags/{slug}/posts', 'Hashtag\PostsByHashtag');
    Route::get('/users/{user}/hashtags', 'Hashtag\UsedHashtags');
    Route::get('/users/{user}/subscriptions', 'User\Subscriptions');

    Route::post('/hashtags/{tag}/subscribers', 'Hashtag\Subscribe');
    Route::delete('/hashtags/{tag}/subscribers', 'Hashtag\Unsubscribe');

    Route::post('/places/{place}/subscribers', 'Subscription\Subscribe@toPlace');
    Route::delete('/places/{place}/subscribers', 'Subscription\Unsubscribe@fromPlace');

    Route::get('/users/{user}/posts', 'Post\UserPosts');

    Route::get('/places/{place}/posts', 'Post\SearchController@showPostsByPlace');
    Route::get('/trips/{trip}/posts', 'Post\SearchController@showPostsByTrip');

    Route::post('/password/change', 'Account\ChangePassword');

    Route::patch('/notifications/{notification}', 'Notifications\Update');
});

Route::get('/search', 'Search');

Route::group(['namespace' => 'Users'], function () {
    Route::get('/users/{username}', 'GetController');
    Route::get('/users', 'ListController');
});

Route::get('/lookup/{id}', 'LookupController');

Route::group(['namespace' => 'Trips'], function () {
    Route::get('/trips', 'ListController');
    Route::get('/users/{userId}/trips', 'ListController');
});

Route::group(['namespace' => 'Account'], function () {
    Route::post('/account/register', 'RegisterController');
});

Route::get('/places', 'Place\Index');
Route::get('/hashtags', 'Hashtag\Index');
Route::get('/countries', 'Country\Autocomplete');
Route::get('/countries/{country}', 'Country\Show');
Route::get('/countries/{country}/cities', 'City\Autocomplete');

Route::group(['namespace' => 'Places'], function () {
    Route::get('/places/search', 'ListController');

    Route::get('/places/{id}', 'GetController');
});

Route::group(['namespace' => 'Email'], function() {
    Route::post('/verification/codes', 'ObtainEmail');
    Route::post('/verification/checks', 'VerifyEmail');
});

Route::group(['namespace' => 'User'], function() {
   Route::post('/account/users', 'SignUpUser');
});

Route::group(['namespace' => 'Account'], function() {
    Route::post('/password/forgot', 'ForgotPassword');
    Route::post('/password/reset', 'ResetPassword');
});


