<?php

namespace tests\Feature\Posts;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Palmy\Media;
use Palmy\Trip;
use Palmy\User;
use Tests\TestCase;

class CreateControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function dataProvider()
    {
        $this->createApplication();

        $user = factory(User::class)->create();

        $trip = factory(Trip::class)->create();

        /** @var \Illuminate\Support\Collection $medias */
        $medias = factory(Media::class, 5)->create();

        return [
            [
                $user,
                [
                    'message' => 'text',
                    'private' => true,
                ],
                [
                    'message' => 'text',
                    'author' => $user->id
                ]
            ],
            [
                $user,
                [
                    'message' => 'text',
                    'private' => false,
                    'participants' => $participants = factory(User::class, 2)->create()->map(function ($participant) {
                        return $participant->id;
                    })
                ],
                [
                    'message' => 'text',
                    'participants' => $participants,
                    'author' => $user->id,
                ]
            ],
            [
                $user,
                [
                    'message' => 'Test Place',
                    'private' => true,
                    'place' => 'google|ChIJybDUc_xKtUYRTM9XV8zWRD0',
                    'trip' => null,
                ],
                [
                    'message' => 'Test Place',
                    'place' => 'google|ChIJybDUc_xKtUYRTM9XV8zWRD0',
                    'author' => $user->id,
                    'trip' => null,
                ]
            ],
            [
                $user,
                [
                    'message' => 'Test Trip',
                    'private' => true,
                    'trip' => $trip->id,
                ],
                [
                    'message' => 'Test Trip',
                    'author' => $user->id,
                    'trip' => $trip->id,
                ]
            ],
            [
                $user,
                [
                    'message' => 'Test Photos',
                    'private' => false,
                    'photos' => $medias->map(function(Media $media) { return $media->id; }),
                ],
                [
                    'message' => 'Test Photos',
                    'photoLinks' => $medias->map(function(Media $media) { return $media->url; })->toArray(),
                ]
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     */
    public function testCreate(User $user, array $data, array $result): void
    {
        $response = $this->actingAs($user)->postJson('/posts', $data);

        $response->assertStatus(201);
        $response->assertJsonFragment($result);
    }
}
