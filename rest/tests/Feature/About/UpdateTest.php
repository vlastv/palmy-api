<?php

namespace Tests\Feature\About;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Palmy\User;
use Tests\TestCase;

class UpdateTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     */
    public function testUpdateFirstLastNames()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->patchJson('/about', [
            'username' => $user->username,
            'firstName' => 'First',
            'lastName' => 'Last',
        ]);

        $response->assertStatus(200);

        $response = $this->actingAs($user)->getJson('/about');

        $response->assertJsonFragment([
            'firstName' => 'First',
            'lastName' => 'Last'
        ]);
    }
}
