<?php

namespace Tests\Feature\Trips;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Palmy\Media;
use Palmy\Trip;
use Palmy\User;
use Tests\TestCase;

class CreateControllerTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * A basic test example.
     */
    public function testMinimal()
    {
        $user = factory(User::class)->create();

        $data = [
            'name' => 'Name of Trip',
            'startDate' => ($start = new \DateTime('-1 year midnight'))->format('Y-m-d'),
            'private' => false,
        ];

        $response = $this->actingAs($user)->postJson('/trips', $data);

        $result = [
            'name' => 'Name of Trip',
            'startDate' => $start->format('Y-m-d'),
            'private' => false,
            'ownedByMe' => true,
        ];

        $response->assertStatus(201);
        $response->assertJsonFragment($result);

        $data = [
            'name' => 'Name of Trip',
            'startDate' => ($start = new \DateTime('-1 year midnight'))->format('Y-m-d'),
            'private' => false,
            'cover' => null,
            'place' => null,
            'participants' => []
        ];

        $response = $this->actingAs($user)->postJson('/trips', $data);
        $response->assertStatus(201);
        $response->assertJsonFragment($result);
    }

    public function testFull()
    {
        $user = factory(User::class)->create();

        $participant = factory(User::class)->create();

        $cover = factory(Media::class)->create();

        $command = [
            'name' => 'Name of Trip',
            'startDate' => (new \DateTime('-2 year midnight'))->format('Y-m-d'),
            'endDate' => (new \DateTime('-1 year midnight'))->format('Y-m-d'),
            'private' => true,
            'participants' => [$participant['id']],
            'cover' => $cover['id'],
            'place' => 'google|ChIJybDUc_xKtUYRTM9XV8zWRD0'
        ];

        $result = [
            'name' => 'Name of Trip',
            'startDate' => (new \DateTime('-2 year midnight'))->format('Y-m-d'),
            'endDate' => (new \DateTime('-1 year midnight'))->format('Y-m-d'),
            'private' => true,
            'coverLink' => $cover['url'],
            'participants' => [$participant['id']],
            'author' => $user->id,
            'place' => 'google|ChIJybDUc_xKtUYRTM9XV8zWRD0'
        ];

        $response = $this->actingAs($user)->postJson('/trips', $command);

        $response->assertStatus(201);
        $response->assertJsonFragment($result);
    }
}
