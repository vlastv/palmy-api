<?php

namespace Tests\Feature\Trips;


use Illuminate\Foundation\Testing\DatabaseMigrations;
use Palmy\Trip;
use Palmy\User;
use Tests\TestCase;

class GetControllerTest extends TestCase
{
    use DatabaseMigrations;

    public function testSuccess()
    {
        $user = factory(User::class)->create();

        $trip = factory(Trip::class)->create();

        $response = $this->actingAs($user)->getJson('/trips/'.$trip->id);

        $response->assertStatus(200);
    }
}
