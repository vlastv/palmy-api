<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Palmy\User;
use Tests\TestCase;

class AboutTest extends TestCase
{
    use DatabaseMigrations;

    public function testAbout()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user, 'api')->getJson('/about');

        $response->assertStatus(200);
    }

    public function testAnonymous()
    {
        $response = $this->getJson('/about');

        $response->assertStatus(401);
    }
}
