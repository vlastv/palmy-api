<?php

namespace Tests\Unit\Canonicalizer;

use Palmy\Canonicalizer\EmailCanonicalizer;
use Tests\TestCase;

class EmailCanonicalizerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testCanonicalize()
    {
        $canonicalizer = new EmailCanonicalizer();

        $expected = 'test@domain.com';

        $this->assertEquals($expected, $canonicalizer->canonicalize('TEST@DOMAIN.COM'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('TEST@domain.COM'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('tEst@DoMaiN.COM'));

        $this->assertEquals('test+suffix@domain.com', $canonicalizer->canonicalize('test+suffix@domain.com'));
        $this->assertEquals('test+suffix@domain', $canonicalizer->canonicalize('test+suffix@domain'));
    }

    public function testGoogleCanonicalize()
    {
        $canonicalizer = new EmailCanonicalizer();

        $expected = 'user@gmail.com';

        $this->assertEquals($expected, $canonicalizer->canonicalize('user+suffix@gmail.com'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('us.er@gmail.com'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('us.er+suffix@gmail.com'));
    }

    public function testNull()
    {
        $canonicalizer = new EmailCanonicalizer();

        $this->assertNull($canonicalizer->canonicalize(null));
    }
}
