<?php

namespace Tests\Unit\Canonicalizer;

use Palmy\Canonicalizer\EmailCanonicalizer;
use Palmy\Canonicalizer\FacebookCanonicalizer;
use Tests\TestCase;

class FacebookCanonicalizerTest extends TestCase
{
    /**
     * A basic test example.
     */
    public function testCanonicalize()
    {
        $canonicalizer = new FacebookCanonicalizer();

        $expected = 'username';

        $this->assertEquals($expected, $canonicalizer->canonicalize('username'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('user.na.me'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('USERNAME'));
        $this->assertEquals($expected, $canonicalizer->canonicalize('USER.NAME'));
    }
}
