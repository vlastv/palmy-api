Setup Heroku
------------

```
heroku git:remote --remote heroku-rest --app palmy-api-dev
heroku git:remote --remote heroku-graphql --app palmy-graphql-dev
```

Deployment to Heroku
--------------------

```
make deploy
```
