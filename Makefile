deploy:
	git subtree push --prefix=rest heroku-rest master
	git subtree push --prefix=graphql heroku-graphql master
	heroku run --remote heroku-rest php artisan migrate
	git push zavarka master
.PHONY: deploy

deps:
	docker run --rm --volume $(PWD)/graphql:/src --workdir /src node:alpine yarn install
	docker run --rm --volume $(PWD)/rest:/app --workdir /app composer install --ignore-platform-reqs --no-scripts
.PHONE: deps

serve:
	docker-compose -p palmy up -d --build
.PHONY: serve

logs:
	docker-compose -p palmy logs -f --tail 0
.PHONY: logs

logs-graphql:
	docker-compose -p palmy logs -f --tail 0 graphql
.PHONY: logs-graphql

logs-rest:
	docker-compose -p palmy logs -f --tail 0 rest
.PHONY: logs-rest

cli:
	docker-compose -p palmy exec rest sh
.PHONY: cli

migrate:
	docker-compose -p palmy exec rest php artisan migrate
.PHONY: migrate

up: deps serve migrate
.PHONY: up

cs:
	docker-compose -p palmy exec rest ./vendor/bin/php-cs-fixer fix --config=.php_cs.dist -v --path-mode=intersection app/
.PHONY:cs
