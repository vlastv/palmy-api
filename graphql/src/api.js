import fetch from 'node-fetch'
import { URL } from 'url'
import querystring from 'querystring';

const BASE_URL = process.env.API_BASE_URL;

export default class Client {
    constructor(token) {
        this.token = token;
    }

    get(path, params) {
        return this.fetch(
            this.constructor.endpoint(path, params),
            {
                method: 'GET'
            }
        );
    }

    post(path, body, params) {
        console.log(body);
        return this.fetch(
            this.constructor.endpoint(path, params),
            {
                body: JSON.stringify(body),
                method: 'POST',
            }
        );
    }

    put(path, body, params) {
        console.log(body);
        return this.fetch(
            this.constructor.endpoint(path, params),
            {
                body: JSON.stringify(body),
                method: 'PUT',
            }
        );
    }

    delete(path, params) {
        return this.fetch(this.constructor.endpoint(path, params), {
            method: 'DELETE',
        });
    }

    patch(path, body, params) {
        console.log(body);
        return this.fetch(this.constructor.endpoint(path, params), {
            body: JSON.stringify(body),
            method: 'PATCH',
        });
    }

    fetch(input, init) {
        init.headers = init.headers || {};
        init.headers['Accept'] = 'application/json';
        init.headers['Content-Type'] = 'application/json';
        init.headers['Authorization'] = this.token;

        return fetch(input, init)
            .then(res => {
                console.log(input, res.status);
                if (res.status === 401) {
                    throw new Error("Access allowed only for registered users")
                }

                if (res.status === 404) {
                    throw new Error();
                }

                if (res.status === 405) {
                    console.log(res.text().then(text => console.log(text)));
                }

                if (res.status === 204) {
                    return null
                }

                if (res.status === 403) {
                    throw new Error('notAuthorized')
                }

                if (res.status === 204) {
                    return null
                }

                return res.json().then(json => {
                    if (res.status === 422) {
                        console.log(input, init.body || {}, json)
                    }

                    return json
                });
            });
        ;
    }

    static endpoint(path, params) {
        let url = new URL(path, BASE_URL);
        let qs = querystring.stringify(params);

        url.search = qs.toString();

        return url.toString()
    }
}
