import express from 'express';
import graphqlHTTP from 'express-graphql'
import Schema from './schema';
import Client from './api'
import DataLoader from 'dataloader';

let app = express();

app.set('port', (process.env.PORT || 5000));

function createDataLoader(client) {
    const userLoader = new DataLoader(ids => Promise.all(
        ids.map(id => client.get(`/users/${id}`))
    ));

    const followersLoader = new DataLoader(ids => Promise.all(
        ids.map(id => client.get(`/users/${id}/followers`).then(json => {
            for (let user in json.users) {
                userLoader.prime(user.id, user)
            }

            return json.users.map(user => user.id)
        }))
    ));

    const placeLoader = new DataLoader(ids => Promise.all(
        ids.map(id => client.get(`/places/${id}`))
    ));

    const postLoader = new DataLoader(ids => Promise.all(ids.map(id => client.get(`/posts/${id}`))));
    const tripLoader = new DataLoader(ids => Promise.all(ids.map(id => client.get(`/trips/${id}`))));

    const apiLoader = new DataLoader(eps => Promise.all(eps.map(ep => client.get(ep))));

    const restLoader = new DataLoader(queries => Promise.all(queries.map(query => {
        if (typeof query === 'string') {
            return client.get(query);
        }

        return client.get(query[0], query[1])
    })), {
        cacheKeyFn: (query) => typeof query === 'string' ? query : JSON.stringify(query)
    });

    return {
        user: userLoader,
        place: placeLoader,
        followers: followersLoader,
        post: postLoader,
        trip: tripLoader,
        api: apiLoader,
        rest: restLoader,
    }
}

app.use('/graphql', graphqlHTTP((req, res) => {
        const client = new Client(req.header('authorization') || "Bearer " + req.query["access_token"]);
        return {
            schema: Schema,
            graphiql: true,
            formatError: error => ({
                message: error.message,
                state: error.originalError && error.originalError.state,
                locations: error.locations,
                path: error.path,
            }),
            context: {
                client: client,
                loaders: createDataLoader(client)
            }
        }
    }
));

app.listen(app.get('port'), function () {
    console.log('Graphql is running on port', app.get('port'));
});
