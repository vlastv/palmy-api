import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {nodeInterface} from "./relay"

import Media from './types/Media';
import {NotificationConnection} from "./types/Notification";
import {Commentable} from "./types/Commentable";

export const ResourceLocatableType = new GraphQLInterfaceType({
    name: 'ResourceLocatable',
    fields: {
        resourcePath: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    resolveType(resource) {
        if (resource.kind === 'user') {
            return UserType
        }

        if (resource.kind === 'trip') {
            return TripType
        }
    }
});

export const IdentityType = new GraphQLObjectType({
    name: 'Identity',
    fields: {
        identifier: {type: new GraphQLNonNull(GraphQLString)},
        provider: {type: new GraphQLNonNull(GraphQLString)},
        avatarLink: {type: GraphQLString},
        name: {type: GraphQLString}
    }
});


export const GenderType = new GraphQLEnumType({
    name: 'Gender',
    values: {
        MALE: {value: 'male'},
        FEMALE: {value: 'female'},
    }
});

export const UserType = new GraphQLObjectType({
    name: 'User',
    fields: () => ({
        id: globalIdField('User'),
        username: {
            type: GraphQLString
        },
        email: {
            type: GraphQLString
        },
        firstName: {
            type: GraphQLString
        },
        lastName: {
            type: GraphQLString
        },
        name: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: ({name, firstName, lastName}) => name || [firstName, lastName].filter(function (name) {
                return !!name
            }).join(' ') || ''
        },
        avatarLink: {
            type: GraphQLString,
            resolve: ({avatar}) => avatar
        },
        coverLink: {
            type: GraphQLString,
        },
        cover: {
            type: Media
        },
        phoneNumber: {
            type: GraphQLString,
            resolve: ({phone}, args, context) => {
                if (phone) {
                    return phone.number
                }
            }
        },
        country: {
            type: GraphQLString
        },
        city: {
            type: GraphQLString
        },
        birthday: {
            type: GraphQLString,
            resolve: ({birthdayDate}) => birthdayDate || undefined
        },
        isViewer: {
            type: new GraphQLNonNull(GraphQLBoolean),
            resolve: (user => user.me)
        },
        viewerRequestedFriendship: {
            type: GraphQLBoolean
        },
        viewerReceivedFriendship: {
            type: GraphQLBoolean
        },
        resourcePath: {
            type: new GraphQLNonNull(GraphQLString)
        },
        gender: {
            type: GenderType
        },
        identities: {
            type: new GraphQLList(new GraphQLNonNull(IdentityType))
        },
        trips: {
            type: TripConnectionType,
            args: {
                ...connectionArgs,
                corpus: {
                    type: TripCorpusType
                }
            },
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/trips`, args])
                .then(resource => {
                    const edges = resource.data.map(node => {
                        return {
                            node: node
                        }
                    });

                    return {
                        totalCount: resource.cursor.count,
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        posts: {
            args: forwardConnectionArgs,
            type: PostConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/posts`, args])
                .then(resource => {
                    const edges = resource.data.map(post => {
                        return {
                            node: post
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        followings: {
            args: connectionArgs,
            type: FollowingConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/followings`, {
                pageSize: args.first
            }])
                .then(json => json.users)
                .then(users => {
                    for (let user in users) {
                        loaders.user.clear(user.id).prime(user.id, user)
                    }

                    return {
                        edges: users
                    }
                })
        },
        followers: {
            type: FollowerConnectionType,
            args: connectionArgs,
            resolve: ({id}, args, {loaders}) => loaders.api.load(`/users/${id}/followers`)
                .then(({data, pagination}) => {
                    const edges = data.map(user => {
                        loaders.user.prime(user.id, user)

                        return user;
                    });

                    return {
                        totalCount: pagination.total,
                        edges: edges,
                    }
                })
        },
        viewerIsFollower: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
        followerCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        followingCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        tripCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        postCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        subscriptions: {
            args: {
                ...forwardConnectionArgs,
                corpus: {
                    type: new GraphQLNonNull(new GraphQLEnumType({
                        name: 'SubscriptionCorpus',
                        values: {
                            HASHTAG: {value: 'hashtag'},
                            TRIP: {value: 'trip'},
                            PLACE: {value: 'place'},
                        }
                    }))
                }
            },
            type: SubscribableConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/subscriptions`, args])
                .then(list => {
                    const edges = list.data.map(resource => {
                        return {
                            node: resource
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!list.cursor.next,
                            endCursor: list.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        feed: {
            args: forwardConnectionArgs,
            type: PublishableConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/feed`, {
                ...args,
                include: 'post'
            }])
                .then(json => {
                    const edges = json.data;

                    return {
                        pageInfo: {
                            hasNextPage: !!json.cursor.next,
                            endCursor: json.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        hashtags: {
            args: forwardConnectionArgs,
            type: HashtagConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/users/${id}/hashtags`, args])
                .then(resource => {
                    const edges = resource.data.map(hashtag => {
                        loaders.rest.prime(`/hashtags/${hashtag.slug}`, hashtag);

                        return {
                            node: hashtag
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        own: {
            type: new GraphQLNonNull(GraphQLBoolean)
        }
    }),
    interfaces: () => ([nodeInterface, ResourceLocatableType, followableType])
});

export const {connectionType: ParticipantConnectionType} = connectionDefinitions({
    nodeType: UserType,
    name: 'Participant'
});

export const {connectionType: FollowerConnectionType} = connectionDefinitions({
    nodeType: UserType,
    name: 'Follower',
    connectionFields: {
        totalCount: {
            type: GraphQLInt
        }
    },
    resolveNode: node => node,
});

export const {connectionType: FollowingConnectionType} = connectionDefinitions({
    nodeType: UserType,
    name: 'Following',
    resolveNode: node => node,
});

export const TripType = new GraphQLObjectType({
    name: 'Trip',
    fields: () => ({
        id: globalIdField('Trip'),
        name: {type: new GraphQLNonNull(GraphQLString)},
        startDate: {type: new GraphQLNonNull(GraphQLString)},
        endDate: {type: GraphQLString},
        photoCount: {type: new GraphQLNonNull(GraphQLInt)},
        videoCount: {type: new GraphQLNonNull(GraphQLInt)},
        participantCount: {type: new GraphQLNonNull(GraphQLInt)},
        likeCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        placeCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        likesCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        coverLink: {type: GraphQLString},
        cover: {
            type: Media
        },
        viewerIsOwner: {
            type: new GraphQLNonNull(GraphQLBoolean),
            resolve: ({ownedByMe}) => ownedByMe
        },
        resourcePath: {
            type: new GraphQLNonNull(GraphQLString)
        },
        participants: {
            type: new GraphQLList(new GraphQLNonNull(UserType)),
            resolve: ({participants}, _, {loaders}) => participants && loaders.user.loadMany(participants)
        },
        author: {
            type: new GraphQLNonNull(UserType),
            resolve: (trip, _, {loaders}) => loaders.user.load(trip.author)
        },
        place: {
            type: PlaceType,
            resolve: (trip, _, {loaders}) => trip.place && loaders.place.load(trip.place)
        },
        viewerHasLiked: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
        private: {
            type: GraphQLBoolean
        },
        isPrivate: {
            type: GraphQLBoolean,
            resolve: post => post.private
        },
        viewerIsSubscriber: {
            type: GraphQLBoolean,
        },
        posts: {
            args: connectionArgs,
            type: PostConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/trips/${id}/posts`, args])
                .then(resource => {
                    const edges = resource.data.map(post => {
                        loaders.post.prime(post.id, post);
                        loaders.rest.prime(`/posts/${post.id}`, post);

                        return {
                            node: post
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        comments: {
            args: connectionArgs,
            type: CommentConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.api.load(`/trips/${id}/comments`)
                .then(({data, pagination}) => {
                    const edges = data.map(comment => {
                        loaders.user.prime(comment.id, comment);

                        return {
                            node: comment
                        };
                    });

                    return {
                        totalCount: pagination.total,
                        edges: edges,
                    }
                })
        }
    }),
    interfaces: () => ([nodeInterface, ResourceLocatableType, LikeableInterface, subscribableType, Commentable, Company])
});

export const PlaceType = new GraphQLObjectType({
    name: 'Place',
    fields: () => ({
        id: globalIdField('Place'),
        name: {type: new GraphQLNonNull(GraphQLString)},
        location: {
            type: new GraphQLObjectType({
                name: 'Location',
                fields: {
                    description: {type: GraphQLString},
                    latitude: {type: GraphQLString},
                    longitude: {type: GraphQLString}
                }
            })
        },
        viewerIsSubscriber: {
            type: GraphQLBoolean
        },
        coverLink: {
            type: GraphQLString
        },
        cover: {
            type: Media
        },
        posts: {
            args: forwardConnectionArgs,
            type: PostConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.rest.load([`/places/${id}/posts`, args])
                .then(resource => {
                    const edges = resource.data.map(post => {
                        loaders.post.prime(post.id, post);
                        loaders.rest.prime(`/posts/${post.id}`, post);

                        return {
                            node: post
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        }
    }),
    interfaces: () => [nodeInterface, subscribableType]
});

export const {connectionType: PlaceConnection} = connectionDefinitions({
    nodeType: PlaceType
});



export const SearchableType = new GraphQLUnionType({
    name: 'Searchable',
    types: () => [UserType, PlaceType, TripType, HashtagType],
    resolveType: ({kind}) => {
        if (kind === 'place') {
            return PlaceType;
        }

        if (kind === 'user') {
            return UserType;
        }

        if (kind === 'trip') {
            return TripType;
        }

        if (kind === 'hashtag') {
            return HashtagType;
        }
    }
});

export const {edgeType: SearchableEdgeType, connectionType: SearchableConnectionType} = connectionDefinitions({
    nodeType: SearchableType,
    connectionFields: {
        totalCount: {
            type: GraphQLInt
        }
    }
});

export const Company = new GraphQLInterfaceType({
    name: 'Company',
    types: () => [PostType, TripType],
    fields: {
        participants: {
            type: new GraphQLList(new GraphQLNonNull(UserType))
        }
    },
    resolveType: ({kind}) => {
        if (kind === 'trip') {
            return TripType
        }

        if (kind === 'place') {
            return PlaceType;
        }

        if (kind === 'hashtag') {
            return HashtagType;
        }
    }
});

export const ViewerType = new GraphQLObjectType({
    name: 'About',
    fields: {
        notifications: {
            type: NotificationConnection,
            args: connectionArgs,
            resolve: (parent, args, {loaders}) => loaders.rest.load(['/notifications', args]).then(resource => {
                const edges = resource.data.map(resource => ({
                    node: resource
                }));

                return {
                    edges: edges
                }
            })
        },
        user: {
            type: UserType,
            resolve(_, __, {client, loaders}) {
                return client.get('/about').then(user => {
                    loaders.user.prime(user.id, user);

                    return user
                })
            }
        }
    }
});

export const TripCorpusType = new GraphQLEnumType({
    name: 'TripCorpus',
    values: {
        POPULAR: {value: 'popular'},
        NEWEST: {value: 'newest'},
        DISCUSSABLE: {value: 'discussable'},
        EDITORIAL: {value: 'editorial'}
    }
});

export const {
    connectionType: TripConnectionType
} = connectionDefinitions({
    nodeType: TripType,
    connectionFields: {
        totalCount: {
            type: new GraphQLNonNull(GraphQLInt)
        }
    },
});

export const {
    connectionType: UserConnectionType
} = connectionDefinitions({
    nodeType: UserType,
    resolveNode: (resource) => resource
});

export const UserCorpusType = new GraphQLEnumType({
    name: "UserCorpus",
    values: {
        POPULAR: {value: "popular"},
    }
});

export const followableType = new GraphQLInterfaceType({
    name: 'Followable',
    fields: () => ({
        followers: {
            type: FollowerConnectionType
        }
    }),
    resolveType: (obj) => {
        if (obj.kind === 'user') {
            return UserType
        }
    }
});

export const subscribableType = new GraphQLInterfaceType({
    name: 'Subscribable',
    fields: () => ({
        id: {
            type: new GraphQLNonNull(GraphQLID)
        },
        viewerIsSubscriber: {
            type: GraphQLBoolean
        },
    }),
    resolveType: ({kind}) => {
        if (kind === 'trip') {
            return TripType
        }

        if (kind === 'place') {
            return PlaceType;
        }

        if (kind === 'hashtag') {
            return HashtagType;
        }
    }
});

export const {
    connectionType: SubscribableConnectionType
} = connectionDefinitions({
    nodeType: subscribableType
});

export const PostType = new GraphQLObjectType({
    name: 'Post',
    fields: () => ({
        id: globalIdField('Post'),
        message: {type: GraphQLString},
        publishTime: {type: new GraphQLNonNull(GraphQLString)},
        author: {
            type: new GraphQLNonNull(UserType),
            resolve: (post, _, {loaders}) => loaders.user.load(post.author)
        },
        participants: {
            type: new GraphQLList(new GraphQLNonNull(UserType)),
            resolve: ({participants}, _, {client, loaders}) => participants && loaders.user.loadMany(participants)
        },
        place: {
            type: PlaceType,
            resolve: ({place}, _, {client, loaders}) => place && loaders.place.load(place)
        },
        photoLinks: {
            type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(GraphQLString)))
        },
        trip: {
            type: TripType,
            resolve: ({tripId}, _, {loaders}) => tripId && loaders.trip.load(tripId)
        },
        resourcePath: {
            type: new GraphQLNonNull(GraphQLString)
        },
        likeCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        viewerHasLiked: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
        photos: {
            type: new GraphQLList(Media),
            resolve: post => post.photos.data
        },
        private: {
            type: GraphQLBoolean,
        },
        isPrivate: {
            type: GraphQLBoolean,
            resolve: post => post.private
        },
        comments: {
            args: connectionArgs,
            type: CommentConnectionType,
            resolve: ({id}, args, {loaders}) => loaders.api.load(`/posts/${id}/comments`)
                .then(({data, pagination}) => {
                    const edges = data.map(comment => {
                        loaders.user.prime(comment.id, comment);

                        return {
                            node: comment
                        };
                    });

                    return {
                        totalCount: pagination.total,
                        edges: edges,
                    }
                })
        },
        hashtags: {
            type: new GraphQLList(new GraphQLNonNull(HashtagType)),
            resolve: ({tags}) => tags.data,
        }
    }),
    interfaces: () => ([nodeInterface, ResourceLocatableType, LikeableInterface, Commentable, Company])
});

const {connectionType: PostConnectionType} = connectionDefinitions({
    nodeType: PostType
});

export const LikeableInterface = new GraphQLInterfaceType({
    name: 'Likeable',
    fields: {
        id: globalIdField(),
        likeCount: {
            type: new GraphQLNonNull(GraphQLInt)
        },
        viewerHasLiked: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
    },
    resolveType: (likeable) => {
        if (likeable.kind === 'trip') {
            return TripType;
        }

        if (likeable.kind === 'post') {
            return PostType;
        }

        return null;
    }
});

export const StatusType = new GraphQLObjectType({
    name: 'Status',
    fields: {
        id: globalIdField('Status'),
        message: {type: new GraphQLNonNull(GraphQLString)}
    }
});

export const PublishableType = new GraphQLUnionType({
    name: 'Publishable',
    types: [TripType, PostType, StatusType],
    resolveType: json => {
        if (json.kind === 'trip') {
            return TripType;
        }

        if (json.kind === 'post') {
            return PostType;
        }
    }
});

export const {edgeType: PublishableEdgeType, connectionType: PublishableConnectionType} = connectionDefinitions({
    nodeType: PublishableType,
    edgeFields: {
        hitTime: {
            type: new GraphQLNonNull(GraphQLString)
        },
        fact: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: obj => obj.fact || 'unknown'
        }
    },
    resolveNode: (json, _, {client, loaders}) => {
        if (json.post) {
            return json.post
        }

        if ('postId' in json) {
            return loaders.post.load(json.postId);
        }

        if ('tripId' in json) {
            return loaders.trip.load(json.tripId);
        }

        return null;
    }
});

export const CommentType = new GraphQLObjectType({
    name: 'Comment',
    fields: {
        id: globalIdField('Comment'),
        message: {
            type: new GraphQLNonNull(GraphQLString)
        },
        publishTime: {
            type: new GraphQLNonNull(GraphQLString),
            resolve: ({createdTime}) => createdTime
        },
        author: {
            type: new GraphQLNonNull(UserType),
            resolve: ({authorId}, _, {loaders}) => loaders.user.load(authorId)
        }
    },
    interfaces: [nodeInterface]
});

export const HashtagType = new GraphQLObjectType({
    name: 'Hashtag',
    fields: {
        id: globalIdField(),
        name: {
            type: new GraphQLNonNull(GraphQLString),
        },
        slug: {
            type: new GraphQLNonNull(GraphQLString),
        },
        posts: {
            args: forwardConnectionArgs,
            type: PostConnectionType,
            resolve: ({slug}, args, {loaders}) => loaders.rest.load([`/hashtags/${slug}/posts`, args])
                .then(resource => {
                    const edges = resource.data.map(post => {
                        loaders.post.prime(post.id, post);
                        loaders.rest.prime(`/posts/${post.id}`, post);

                        return {
                            node: post
                        }
                    });

                    return {
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        viewerIsSubscriber: {
            type: GraphQLBoolean
        }
    },
    interfaces: () => [subscribableType]
});

export const {
    connectionType: HashtagConnectionType
} = connectionDefinitions({
    nodeType: HashtagType,
});

export const {
    connectionType: CommentConnectionType
} = connectionDefinitions({
    nodeType: CommentType,
    connectionFields: {
        totalCount: {
            type: new GraphQLNonNull(GraphQLInt)
        }
    }
});

export const ErrorType = new GraphQLObjectType({
    name: 'Error',
    fields: {
        location: {type: GraphQLString},
        reason: {type: GraphQLString}
    }
});
