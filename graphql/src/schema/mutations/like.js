import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {LikeableInterface} from "../types";

export const like = mutationWithClientMutationId({
    name: 'Like',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: LikeableInterface
        }
    },
    mutateAndGetPayload: ({subjectId}, {client}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('Trip' === type) {
            return client.post(`/trips/${id}/likes`).then(collection => ({
                subject: collection
            }))
        }

        if ('Post' === type) {
            return client.post(`/posts/${id}/likes`).then(collection => ({
                subject: collection
            }))
        }

        return {}
    }
});

export const unlike = mutationWithClientMutationId({
    name: 'Unlike',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: LikeableInterface
        }
    },
    mutateAndGetPayload: ({subjectId}, {client}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('Trip' === type) {
            return client.delete(`/trips/${id}/likes`).then(collection => ({
                subject: collection
            }))
        }

        if ('Post' === type) {
            return client.delete(`/posts/${id}/likes`).then(collection => ({
                subject: collection
            }))
        }

        return {}
    }
});
