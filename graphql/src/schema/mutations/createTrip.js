import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType,
    GraphQLInputObjectType
} from 'graphql';

import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { TripType, PublishableEdgeType } from "../types"

export const createTrip = mutationWithClientMutationId({
    name: 'CreateTrip',
    inputFields: {
        name: { type: new GraphQLNonNull(GraphQLString) },
        startDate: {
            type: new GraphQLNonNull(GraphQLString),
            description: 'Format: YYYY-MM-DD'
        },
        endDate: { type: GraphQLString },
        participants: { type: new GraphQLList(GraphQLID) },
        place: {
            type: GraphQLID
        },
        isPrivate: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
        cover: {
            type: GraphQLID,
        },
    },
    outputFields: {
        trip: { type: new GraphQLNonNull(TripType) },
        feedEdge: {
            type: PublishableEdgeType
        }
    },
    mutateAndGetPayload: (command, { client }) => {
        if ('isPrivate' in command) {
            command.private = command.isPrivate;
            delete command.isPrivate;
        }

        command = {
            ...command,
            cover: command.cover ? fromGlobalId(command.cover).id : null,
            participants: command.participants ? command.participants.map(participant => fromGlobalId(participant).id) : null,
            place: command.place ? fromGlobalId(command.place).id : null,
        };

        return client.post('/trips', command)
            .then(json => {
                if (json.error) {

                   throw json.error.errors[0].reason
                }

                return {
                    feedEdge: json,
                    trip: json.post
                }
        })
    }
});

export const updateTrip = mutationWithClientMutationId({
    name: 'UpdateTrip',
    inputFields: {
        tripId: {
            type: new GraphQLNonNull(GraphQLID)
        },
        name: {
            type: GraphQLString
        },
        startDate: {
            type: GraphQLString,
            description: 'Format: YYYY-MM-DD'
        },
        endDate: {
            type: GraphQLString
        },
        participants: {
            type: new GraphQLList(GraphQLID) },
        place: {
            type: GraphQLID
        },
        isPrivate: {
            type: GraphQLBoolean
        },
        cover: {
            type: GraphQLID,
        },
    },
    outputFields: {
        trip: { type: new GraphQLNonNull(TripType) }
    },
    mutateAndGetPayload: (command, { client }) => {
        const { type, id } = fromGlobalId(command.tripId);
        delete command.tripId;

        if ('isPrivate' in command) {
            command.private = command.isPrivate;
            delete command.isPrivate;
        }

        if (type !== 'Trip') {
            throw new Error('Could not resolve to a node with the global id of \''+command.postId+'\'');
        }

        if (command.cover) {
            command.cover = fromGlobalId(command.cover).id;
        }

        if (command.place) {
            command.place = fromGlobalId(command.place).id
        }

        if (command.participants) {
            command.participants = command.participants.map(participant => fromGlobalId(participant).id)
        }

        return client.patch(`/trips/${id}`, command)
            .then(json => {
                if (json.error) {

                    throw json.error.errors[0].reason
                }

                return {
                    trip: json
                }
            })
    }
});

export const deleteTrip = mutationWithClientMutationId({
    name: 'DeleteTrip',
    inputFields: {
        tripId: {
            type: new GraphQLNonNull(GraphQLID),
        },
    },
    outputFields: {

    },
    mutateAndGetPayload: (input, { client, loaders }) => {
        const { type, id } = fromGlobalId(input.tripId);

        if (type !== 'Trip') {
            throw new Error();
        }

        return client.delete(`/trips/${id}`).then(json => ({}));
    }
});
