import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {subscribableType} from "../types";

export const subscribe = mutationWithClientMutationId({
    name: 'Subscribe',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: subscribableType
        }
    },
    mutateAndGetPayload: ({subjectId}, {client}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('Trip' === type) {
            return client.post(`/trips/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        if ('Place' === type) {
            return client.post(`/places/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        if ('Hashtag' === type) {
            return client.post(`/hashtags/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        return {}
    }
});

export const unsubscribe = mutationWithClientMutationId({
    name: 'Unsubscribe',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: subscribableType
        }
    },
    mutateAndGetPayload: ({subjectId}, {client}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('Trip' === type) {
            return client.delete(`/trips/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        if ('Place' === type) {
            return client.delete(`/places/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        if ('Hashtag' === type) {
            return client.delete(`/hashtags/${id}/subscribers`).then(collection => ({
                subject: collection
            }))
        }

        return {}
    }
});
