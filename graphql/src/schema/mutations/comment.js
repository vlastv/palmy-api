import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { CommentType, Commentable } from "../types"

export const addComment = mutationWithClientMutationId({
    name: "AddComment",
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        },
        message: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    outputFields: {
        comment: {
            type: CommentType,
            resolve: resource => resource
        }
    },
    mutateAndGetPayload: ({subjectId, message}, {client}) => {
        const {type, id} = fromGlobalId(subjectId);

        let ep = null;

        if ('Post' === type) {
            ep = `/posts/${id}/comments`
        }

        if ('Trip' === type) {
            ep = `/trips/${id}/comments`
        }

        if (!ep) {
            return;

        }
        return client.post(ep, {
            message: message
        });
    }
});

export const deleteComment = mutationWithClientMutationId({
    name: 'DeleteComment',
    inputFields: {
        id: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    mutateAndGetPayload: ({id: commentId}, {client}) => {
        const {type, id} = fromGlobalId(commentId);

        if (type !== 'Comment') {
            throw new Error();
        }

        return client.delete(`/comments/${id}`).then(() => ({}))
    }
});
