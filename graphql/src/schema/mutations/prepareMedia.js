import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType,
    GraphQLInputObjectType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

const contextEnum = new GraphQLEnumType({
    name: 'FileTarget',
    values: {
        'USER_AVATAR': {},
        'USER_COVER': {},
        'TRIP_COVER': {},
        'POST_PHOTO': {}
    }
});

export default mutationWithClientMutationId({
    name: 'PrepareMedia',
    inputFields: {
        context: {type: new GraphQLNonNull(contextEnum)},
        fileName: {type: GraphQLString},
        fileType: {type: GraphQLString}
    },
    outputFields: {
        id: globalIdField('Media'),
        uploadUrl: {type: GraphQLString},
        viewUrl: {type: GraphQLString}
    },
    mutateAndGetPayload({context, fileName, fileType}, {client}) {
        return client.post(
            '/medias',
            {
                name: fileName,
                mimeType: fileType
            },
            {
                'context': context
            }
        ).then(resource => {
            return resource
        });
    }
});
