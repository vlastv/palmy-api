import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType,
    GraphQLError
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';
import Notification from "../types/Notification";

export default mutationWithClientMutationId({
    name: "MarkReadNotification",
    inputFields: () => ({
        notificationId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    }),
    outputFields: () => ({
        notification: {
            type: Notification
        }
    }),
    mutateAndGetPayload: ({notificationId}, {client}) => {
        const {id} = fromGlobalId(notificationId);

        return client.patch(`/notifications/${id}`, {
            readed: true
        }).then(resource => {
            return {
                notification: resource
            }
        })
    }
})
