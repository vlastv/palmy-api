import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {followableType} from "../types";

export const follow = mutationWithClientMutationId({
    name: 'Follow',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: followableType
        }
    },
    mutateAndGetPayload: ({subjectId}, {client, loaders}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('User' === type) {
            return client.post(`/users/${id}/followers`)
                .then(user => {
                    loaders.user.clear(user.id).prime(user.id, user);

                    return user
                })
                .then(user => ({
                    subject: loaders.user.load(user.id)
                }))
        }

        return {}
    }
});

export const unfollow = mutationWithClientMutationId({
    name: 'Unfollow',
    inputFields: {
        subjectId: {
            type: new GraphQLNonNull(GraphQLID)
        }
    },
    outputFields: {
        subject: {
            type: followableType
        }
    },
    mutateAndGetPayload: ({subjectId}, {client, loaders}) => {
        const {type, id} = fromGlobalId(subjectId);
        if ('User' === type) {
            return client.delete(`/users/${id}/followers`)
                .then(user => {
                    loaders.user.clear(user.id).prime(user.id, user);

                    return user
                })
                .then(user => ({
                    subject: loaders.user.load(user.id)
                }))
        }

        return {}
    }
});
