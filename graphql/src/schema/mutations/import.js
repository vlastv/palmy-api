import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';
import {UserType} from "../types";

export const load = mutationWithClientMutationId({
    name: 'LoadFriends',
    inputFields: {
        provider: {
            type: new GraphQLEnumType({
                name: 'FriendsProvider',
                values: {
                    MAIL: {value: 'mail'}
                }
            })
        },
        username: {
            type: new GraphQLNonNull(GraphQLString)
        },
        password: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    outputFields: {
        users: {
            type: new GraphQLList(new GraphQLNonNull(UserType))
        }
    },
    mutateAndGetPayload: () => {
        return {}
    }
});