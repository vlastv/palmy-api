import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { UserType, ErrorType } from "../types"

export const signupUser = mutationWithClientMutationId({
    name: "SignUpUser",
    inputFields: {
        username: {
            type: GraphQLString
        },
        firstName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        lastName: {
            type: new GraphQLNonNull(GraphQLString)
        },
        password: {
            type: new GraphQLNonNull(GraphQLString)
        },
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
        code: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    outputFields: {
        user: {
            type: UserType
        },
        errors: {
            type: new GraphQLList(new GraphQLNonNull(ErrorType))
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/account/users', input).then(res => {
            const {error} = res;
            if (error) {
                return {
                    errors: error.errors,
                }
            }

            return {
                user: res
            };
        })
    }
});

export const verifyEmail = mutationWithClientMutationId({
    name: "VerifyEmail",
    inputFields: {
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
        code: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    outputFields: {
        errors: {
            type: new GraphQLList(new GraphQLNonNull(ErrorType))
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/verification/checks', input).then((res) => {
            console.log(res);
            const {error} = res;
            if (error) {
                return {
                    errors: error.errors,
                }
            }

            return res;
        })
    }
});

export const obtainEmail = mutationWithClientMutationId({
    name: "ObtainEmail",
    inputFields: {
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
    },
    outputFields: {
        errors: {
            type: new GraphQLList(new GraphQLNonNull(ErrorType))
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/verification/codes', input).then((res) => {
            const {error} = res;
            if (error) {
                return {
                    errors: error.errors,
                }
            }

            return res;
        })
    }
});
