import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { UserType } from "../types"

export default mutationWithClientMutationId({
    name: "LinkAccount",
    inputFields: {
        provider: {
            type: new GraphQLNonNull(GraphQLString)
        },
    },
    outputFields: {
        user: {
            type: UserType
        }
    },
    mutateAndGetPayload: (input, {client}) => {

    }
});
