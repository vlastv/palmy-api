import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {
    GenderType,
    UserType,
    ErrorType
} from "../types"

export default mutationWithClientMutationId({
    name: "UpdateProfile",
    inputFields: {
        username: { type: GraphQLString },
        firstName: { type: GraphQLString },
        lastName: { type: GraphQLString },
        email: { type: GraphQLString },
        phoneNumber: { type: GraphQLString },
        country: { type: GraphQLString },
        city: { type: GraphQLString },
        gender: { type: GenderType },
        birthday: { type: GraphQLString },
        avatar: { type: GraphQLID },
        cover: { type: GraphQLID },
    },
    outputFields: {
        user: {
            type: UserType
        },
        errors: {
            type: new GraphQLList(new GraphQLNonNull(ErrorType))
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.patch('/about', {
            ...input,
            birthdayDate: input.birthday,
            phone: {
                number: input.phoneNumber
            },
            avatar: input.avatar && fromGlobalId(input.avatar, 'Media').id,
            cover: input.cover && fromGlobalId(input.cover, 'Media').id
        }).then(json => {
            return {
                error: json.error,
                user: json
            }
        })
    }
});
