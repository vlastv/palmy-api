import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType,
    GraphQLError
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

export const forgotPassowrd = mutationWithClientMutationId({
    name: 'SendResetPasswordToken',
    inputFields: {
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/password/forgot', input).then(response => ({}))
    }
});

export const resetPassword = mutationWithClientMutationId({
    name: 'ResetPassword',
    inputFields: {
        token: {
            type: new GraphQLNonNull(GraphQLString)
        },
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
        password: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/password/reset', input).then(response => ({}))
    }
});

class ValidationError extends GraphQLError {
    constructor(message, errors) {
        super(message);

        if (!errors) {
            return;
        }

        console.log('----', message, errors, '----');

        this.state = errors.reduce((result, error) => {
            if (Object.prototype.hasOwnProperty.call(result, error.key)) {
                result[error.key].push(error.message);
            } else {
                result[error.key] = [error.message];
            }
            return result;
        }, {});
    }
}

export const changePassword = mutationWithClientMutationId({
    name: 'ChangePassword',
    inputFields: {
        currentPassword: {
            type: new GraphQLNonNull(GraphQLString)
        },
        newPassword: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.post('/password/change', {
            current: input.currentPassword,
            password: input.newPassword,
        }).then(response => {
            if (!response) {
                return {}
            }

            const fieldMap = {
                'current': 'currentPassowrd',
                'password': 'newPassword',
            };

            if ('error' in response) {
                const err = response.error;
                throw new ValidationError(err.message, err.errors && err.errors.map((error) => {
                    return {
                        key: fieldMap[error.location],
                        message: error.reason
                    }
                }))
            }

            return {}
        })
    }
});
