import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { UserType } from "../types"

export default mutationWithClientMutationId({
    name: "ConfirmEmail",
    inputFields: {
        email: {
            type: new GraphQLNonNull(GraphQLString)
        },
        token: {
            type: new GraphQLNonNull(GraphQLString)
        }
    },
    outputFields: {
        user: {
            type: UserType
        }
    },
    mutateAndGetPayload: (input, {client}) => {
        return client.put('/userinfo', input).then(response => {
            return {
                error: response.error,
                user: response
            }
        })
    }
});
