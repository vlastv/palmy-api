import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import { PostType, PublishableEdgeType } from "../types"


export const publishPost = mutationWithClientMutationId({
    name: 'PublishPost',
    inputFields: {
        message: {
            type: GraphQLString
        },
        participants: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        },
        place: {
            type: GraphQLID
        },
        photos: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        },
        isPrivate: {
            type: new GraphQLNonNull(GraphQLBoolean)
        },
        trip: {
            type: GraphQLID
        }
    },
    outputFields: {
        feedEdge: {
            type: PublishableEdgeType
        },
        post: {
            type: new GraphQLNonNull(PostType)
        }
    },
    mutateAndGetPayload: (command, { client }) => {
        if ('isPrivate' in command) {
            command.private = command.isPrivate;
            delete command.isPrivate;
        }

        command = {
            ...command,
            photos: (command.photos || []).map(photo => fromGlobalId(photo).id),
            participants: command.participants ? command.participants.map(participant => fromGlobalId(participant).id) : null,
            place: command.place ? fromGlobalId(command.place).id : null,
            trip: command.trip ? fromGlobalId(command.trip).id : null,
        };

        return client.post('/posts', command)
            .then(json => {
                if (json.error) {

                    throw json.error.errors[0].reason
                }

                return {
                    feedEdge: json,
                    post: json.post
                }
            })
    }
});

export const updatePost = mutationWithClientMutationId({
    name: 'UpdatePost',
    inputFields: {
        postId: {
            type: new GraphQLNonNull(GraphQLID)
        },
        message: {
            type: GraphQLString
        },
        participants: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        },
        place: {
            type: GraphQLID
        },
        photos: {
            type: new GraphQLList(new GraphQLNonNull(GraphQLID))
        },
        isPrivate: {
            type: GraphQLBoolean
        },
        trip: {
            type: GraphQLID
        }
    },
    outputFields: {
        post: {
            type: new GraphQLNonNull(PostType)
        }
    },
    mutateAndGetPayload: (command, { client }) => {
        const { type, id } = fromGlobalId(command.postId);
        delete command.postId;

        if ('isPrivate' in command) {
            command.private = command.isPrivate;
            delete command.isPrivate;
        }

        if (type !== 'Post') {
            throw new Error('Could not resolve to a node with the global id of \''+command.postId+'\'');
        }

        if (command.photos) {
            command.photos = command.photos.map(photo => fromGlobalId(photo).id)
        }

        if (command.participants) {
            command.participants = command.participants.map(participant => fromGlobalId(participant).id)
        }

        if (command.place) {
            command.place = fromGlobalId(command.place).id
        }

        if (command.trip) {
            command.trip = fromGlobalId(command.trip).id
        }

        return client.patch(`/posts/${id}`, command)
            .then(json => {
                if (json.error) {

                    throw json.error.errors[0].reason
                }

                return {
                    post: json
                }
            })
    }
});

export const deletePost = mutationWithClientMutationId({
    name: 'DeletePost',
    inputFields: {
        postId: {
            type: new GraphQLNonNull(GraphQLID),
        },
    },
    outputFields: {

    },
    mutateAndGetPayload: (input, { client, loaders }) => {
        const { type, id } = fromGlobalId(input.postId);

        if (type !== 'Post') {
            throw new Error();
        }

        return client.delete(`/posts/${id}`).then(json => ({}));
    }
});
