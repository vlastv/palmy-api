import {
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString
} from 'graphql';
import {Company} from "../types";
import Notification, {notificationInterfaceFields} from "./Notification";

const NotifyParticipant = new GraphQLObjectType({
    name: 'NotifyParticipant',
    fields: () => ({
        ...notificationInterfaceFields,
        subject: {
            type: Company
        }
    }),
    interfaces: () => [Notification]
});

export default NotifyParticipant;
