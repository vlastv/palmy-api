import {
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import {notificationInterfaceFields} from "./Notification";
import Notification from "./Notification";
import {Commentable} from "./Commentable";
import {CommentType} from "../types";

const NotifyComment = new GraphQLObjectType({
    name: 'NotifyComment',
    fields: () => ({
        ...notificationInterfaceFields,
        comment: {
            type: CommentType,
            resolve: ({data}) => data.comment
        },
        subject: {
            type: Commentable,
            resolve: ({data}) => data.subject
        }
    }),
    interfaces: () => [Notification]
});

export default NotifyComment;
