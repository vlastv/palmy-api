import {
    GraphQLID,
    GraphQLInterfaceType,
    GraphQLList,
    GraphQLNonNull,
    GraphQLUnionType,
    GraphQLBoolean,
    GraphQLString
} from 'graphql';

import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import NotifyLike from "./NotifyLike";
import NotifyComment from "./NotifyComment";
import NotifyParticipant from "./NotifyParticipant";

export const notificationInterfaceFields = {
    id: globalIdField('Notification', ({id}) => id),
    readByViewer: {
        type: GraphQLBoolean,
        resolve: ({readByMe}) => readByMe
    },
    readByViewerTime: {
        type: GraphQLString,
        resolve: ({readByMeTime}) => readByMeTime
    }
};

const Notification = new GraphQLInterfaceType({
    name: 'Notification',
    fields: () => notificationInterfaceFields,
    resolveType: (resource) => {
        const lookup = {
            ['NewComment']: NotifyComment,
            ['NewLike'] : NotifyLike,
        };

        return lookup[resource.type];
    },
});

const {connectionType} = connectionDefinitions({
    nodeType: Notification
});

export const NotificationConnection = connectionType;
export default Notification;
