import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

const City = new GraphQLObjectType({
    name: "City",
    fields: () => ({
        id: globalIdField(),
        name: {
            type: GraphQLString
        },
        regionName: {
            type: GraphQLString
        },
        areaName: {
            type: GraphQLString
        }
    })
});

const {connectionType} = connectionDefinitions({
    nodeType: City
});

export const CityConnection = connectionType;

export default City;
