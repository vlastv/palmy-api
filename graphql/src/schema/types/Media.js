import {
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} from 'graphql';


import {
    globalIdField,
} from 'graphql-relay';

import { URL } from 'url';

const Media = new GraphQLObjectType({
    name: 'Media',
    fields: {
        id: globalIdField(),
        url: {
            args: {
                width: {
                    type: GraphQLInt
                },
                height: {
                    type: GraphQLInt
                }
            },
            type: new GraphQLNonNull(GraphQLString),
            resolve: (media, {width: w, height: h}) => {
                const url = new URL(media.viewUrl);

                w = w || '';
                h = h || '';

                if (w || h) {
                    url.pathname = `/${w}x${h}`+url.pathname;
                }

                return url.toString()
            }
        }
    }
});

export default Media;
