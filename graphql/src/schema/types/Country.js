import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {CityConnection} from "./City";
import {nodeInterface} from "../relay";

const Country = new GraphQLObjectType({
    name: "Country",
    fields: () => ({
        id: globalIdField(),
        name: {
            type: GraphQLString
        },
        cities: {
            args: {
                ...forwardConnectionArgs,
                query: {
                    type: GraphQLString
                }
            },
            type: CityConnection,
            resolve: (country, args, {loaders}) => loaders.rest.load([`/countries/${country.id}/cities`, args])
                .then(res => {
                    const edges = res.data.map(city => {
                        return {
                            node: city
                        }
                    });

                    return {
                        edges: edges
                    }
                })
        }
    }),
    interfaces: () => [nodeInterface]
});

const {connectionType} = connectionDefinitions({
    nodeType: Country
});

export const CountryConnection = connectionType;

export default Country;
