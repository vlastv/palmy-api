import {
    GraphQLID,
    GraphQLInterfaceType,
    GraphQLList,
    GraphQLNonNull,
    GraphQLUnionType,
} from 'graphql';

import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';
import {CommentConnectionType, PostType, TripType} from "../types";

export const Commentable = new GraphQLInterfaceType({
    name: 'Commentable',
    fields: () => ({
        id: {
            type: GraphQLID
        },
        comments: {
            type: CommentConnectionType
        }
    }),
    resolveType: ({kind}) => {
        if ('trip' === kind) {
            return TripType
        }

        if ('post' === kind) {
            return PostType
        }

        return null
    }
});
