import {
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString
} from 'graphql';

import {LikeableInterface} from "../types";
import Notification, {notificationInterfaceFields} from "./Notification";

const NotifyLike = new GraphQLObjectType({
    name: 'NotifyLike',
    fields: () => ({
        ...notificationInterfaceFields,
        subject: {
            type: LikeableInterface,
            resolve: ({data}) => data.subject,
        }
    }),
    interfaces: () => [Notification]
});

export default NotifyLike;
