import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType,
    GraphQLInputObjectType
} from 'graphql';

import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
    pluralIdentifyingRootField
} from 'graphql-relay';

import markReadNotification from './mutations/markReadNotification';

export default new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
        recoveryPassword: require('./mutations/recoveryPassword').default,
        signup: require('./mutations/signup').default,
        updateProfile: require('./mutations/updateProfile').default,
        confirmEmail: require('./mutations/confirmEmail').default,
        linkAccount: require('./mutations/linkAccount').default,
        prepareMedia: require('./mutations/prepareMedia').default,
        addComment: require('./mutations/comment').addComment,
        deleteComment: require('./mutations/comment').deleteComment,
        publishPost: require('./mutations/publishPost').publishPost,
        updatePost: require('./mutations/publishPost').updatePost,
        createTrip: require('./mutations/createTrip').createTrip,
        updateTrip: require('./mutations/createTrip').updateTrip,
        like: require('./mutations/like').like,
        unlike: require('./mutations/like').unlike,
        follow: require('./mutations/follow').follow,
        unfollow: require('./mutations/follow').unfollow,
        subscribe: require('./mutations/subscribe').subscribe,
        unsubscribe: require('./mutations/subscribe').unsubscribe,
        deletePost: require('./mutations/publishPost').deletePost,
        deleteTrip: require('./mutations/createTrip').deleteTrip,
        verifyEmail: require('./mutations/user').verifyEmail,
        obtainEmail: require('./mutations/user').obtainEmail,
        signupUser: require('./mutations/user').signupUser,
        sendResetPasswordToken: require('./mutations/password').forgotPassowrd,
        resetPassword: require('./mutations/password').resetPassword,
        changePassword: require('./mutations/password').changePassword,
        loadFriends: require('./mutations/import').load,
        markReadNotification,
    })
});
