import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    forwardConnectionArgs,
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import {
    nodeField
} from "./relay"

import {
    UserType,
    ResourceLocatableType,
    ViewerType,
    TripConnectionType,
    TripCorpusType,
    UserConnectionType,
    UserCorpusType,
    SearchableConnectionType,
    PublishableConnectionType, HashtagType, PlaceConnection, HashtagConnectionType
} from "./types"
import {CountryConnection} from "./types/Country";
import Country from "./types/Country";

export default new GraphQLObjectType({
    name: 'Root',
    fields: {
        viewer: {
            type: ViewerType,
            resolve: () => ({})
        },
        node: nodeField,
        user: {
            type: UserType,
            args: {
                username: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve: (parent, {username}, {client}) => {
                return client.get(`/users/${username}`).then(user => {
                    if (!user) {
                        throw new Error(`Could not resolve to a User with the login of '${username}'.`)
                    }

                    return user
                })
            }
        },
        resource: {
            type: ResourceLocatableType,
            args: {
                url: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            resolve(parent, {url}, {client}) {
                if (url[0] !== '/') {
                    url = '/' + url
                }

                return client.get(`/lookup${url}`).then(data => {
                    if (!data) {
                        throw new Error(`Could not locate resource at '${url}'`)
                    }

                    return data
                })
            }
        },
        trips: {
            type: TripConnectionType,
            args: {
                ...connectionArgs,
                corpus: {
                    type: TripCorpusType
                }
            },
            resolve(_, args, {loaders}) {
                return loaders.rest.load(['/trips', args])
                    .then(resource => {
                        const edges = resource.data.map(node => {
                            return {
                                node: node
                            }
                        });

                        return {
                            totalCount: resource.cursor.count,
                            pageInfo: {
                                hasNextPage: !!resource.cursor.next,
                                endCursor: resource.cursor.next
                            },
                            edges: edges
                        }
                    })
            }
        },
        users: {
            type: UserConnectionType,
            args: {
                ...forwardConnectionArgs,
                sort: {
                    type: UserCorpusType
                },
                corpus: {
                    type: UserCorpusType,
                }
            },
            resolve: (parent, args, {loaders}) => {
                return loaders.rest.load(['/users', args]).then(resource => {
                    const edges = resource.data.map(user => {

                        return user;
                    });

                    return {
                        totalCount: resource.cursor.count,
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                })
            }
        },
        suggestions: {
            args: {
                ...forwardConnectionArgs,

                text: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                corpus: {
                    type: new GraphQLNonNull(new GraphQLEnumType({
                        name: 'SuggestionCorpus',
                        values: {
                            'PLACE': {value: 'place'}
                        }
                    }))
                }
            },
            type: SearchableConnectionType,
            resolve: (root, {text, corpus}, {loaders}) => {
                if (corpus === 'place') {
                    return loaders.rest.load(['/places/search', {
                        query: text
                    }]).then(resource => {
                        const edges = resource.data.map(place => {
                            return {
                                node: place
                            }
                        })

                        return {
                            edges: edges
                        }
                    })
                }
            }
        },
        search: {
            args: {
                ...forwardConnectionArgs,
                text: {
                    type: new GraphQLNonNull(GraphQLString)
                },
                corpus: {
                    type: new GraphQLEnumType({
                        name: 'SearchCorpus',
                        values: {
                            'USER': {value: 'users'},
                            'PLACE': {value: 'places'},
                            'TRIP': {value: 'trips'},
                            'HASHTAG': {value: 'hashtags'},
                        }
                    })
                },
            },
            type: SearchableConnectionType,
            resolve: (root, args, {client, loaders}) => {
                return loaders.rest.load(['/search', args]).then(resource => {
                    const edges = resource.data.map(node => {
                        return {
                            node: node.searchable
                        }
                    });

                    return {
                        totalCount: resource.cursor.count,
                        pageInfo: {
                            hasNextPage: !!resource.cursor.next,
                            endCursor: resource.cursor.next
                        },
                        edges: edges
                    }
                });
            }
        },
        feed: {
            args: connectionArgs,
            type: PublishableConnectionType,
            deprecationReason: "Use `feed` in User",
            resolve: (_, args, {client}) =>
                client.get(`/users/me/feed`, {
                    ...args,
                    include: 'post'
                }).then(json => {
                    const edges = json.data;

                    return {
                        pageInfo: {
                            hasNextPage: !!json.cursor.next,
                            endCursor: json.cursor.next
                        },
                        edges: edges
                    }
                })
        },
        hashtag: {
            args: {
                slug: {
                    type: new GraphQLNonNull(GraphQLString)
                }
            },
            type: HashtagType,
            resolve: (_, {slug}, {loaders}) => loaders.rest.load(`/hashtags/${slug}`)
        },
        places: {
            args: {
                ...forwardConnectionArgs,
                sort: {
                    type: new GraphQLEnumType({
                        name: 'PlaceSort',
                        values: {
                            ["POPULARITY"]: {}
                        }
                    })
                }
            },
            type: PlaceConnection,
            resolve: (_, args, {loaders}) => {
                return loaders.rest.load([`/places`, args]).then(response => {
                    const edges = response.data.map(resource => ({
                        node: resource
                    }));

                    return {
                        totalCount: response.cursor.count,
                        pageInfo: {
                            hasNextPage: !!response.cursor.next,
                            endCursor: response.cursor.next
                        },
                        edges: edges
                    }
                })
            }
        },
        hashtags: {
            args: {
                ...forwardConnectionArgs,
                sort: {
                    type: new GraphQLEnumType({
                        name: 'HashtagSort',
                        values: {
                            ["POPULARITY"]: {}
                        }
                    }),
                    defaultValue: "POPULARITY"
                }
            },
            type: HashtagConnectionType,
            resolve: (_, args, {loaders}) => loaders.rest.load(['/hashtags', args]).then(response => {
                console.log(response)
            })
        },
        countries: {
            args: {
                ...forwardConnectionArgs,
                query: {
                    type: GraphQLString
                }
            },
            type: CountryConnection,
            resolve: (_, args, {loaders}) => loaders.rest.load(['/countries', args]).then(res => {
                const edges = res.data.map(country => {
                    return {
                        node: country
                    }
                });

                return {
                    edges: edges
                }
            })
        }
    }
});
