import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';

import RootType from './root';
import MutationType from './mutation'
import NotifyComment from "./types/NotifyComment";
import NotifyParticipant from "./types/NotifyParticipant";
import NotifyLike from "./types/NotifyLike";

export default new GraphQLSchema({
    query: RootType,
    mutation: MutationType,
    types: [
        NotifyComment,
        NotifyParticipant,
        NotifyLike,
    ]
});
