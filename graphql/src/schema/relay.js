import {
    GraphQLBoolean,
    GraphQLID,
    GraphQLInt,
    GraphQLList,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLSchema,
    GraphQLString,
    GraphQLEnumType,
    GraphQLInterfaceType,
    GraphQLUnionType
} from 'graphql';


import {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
    cursorForObjectInConnection,
    fromGlobalId,
    globalIdField,
    mutationWithClientMutationId,
    nodeDefinitions,
    toGlobalId,
} from 'graphql-relay';
import Country from "./types/Country";

export const { idFetcher, typeResolver } = {
    idFetcher:  (globalId, { client, loaders }) => {
        const {type, id} = fromGlobalId(globalId);
        if (type === 'Post') {
            return loaders.post.load(id);
        } else if (type === 'User') {
            return loaders.user.load(id);
        } else if (type === 'Trip') {
            return loaders.trip.load(id);
        } else if (type === 'Place') {
            return loaders.rest.load(`/places/${id}`)
        } else if (type === 'Country') {
            return loaders.rest.load(`/countries/${id}`)
        }
        return null;
    },
    typeResolver: ({ kind }) => {
        if (kind === 'trip') {
            return require('./types').TripType
        }

        if (kind === 'post') {
            return require('./types').PostType
        }

        if (kind === 'user') {
            return require('./types').UserType
        }

        if (kind === 'place') {
            return require('./types').PlaceType
        }

        if (kind === 'country') {
            return Country
        }

        return null;
    }
};

export const {nodeInterface, nodeField} = nodeDefinitions(idFetcher, typeResolver);

